//
//  RandomGarbage_Tests.swift
//  VAMP Tests
//
//  Created by 'Ili Butterfield on 6/26/20.
//  Copyright © 2020 'Ili Butterfield. All rights reserved.
//

import XCTest

class RandomGarbage_Tests: XCTestCase {

    func testFormattingRoundTimestamps() {
        XCTAssertEqual(formatTimestampAsHMS(0), "0:00")
        XCTAssertEqual(formatTimestampAsHMS(1), "0:01")
        XCTAssertEqual(formatTimestampAsHMS(2), "0:02")
        XCTAssertEqual(formatTimestampAsHMS(59), "0:59")
        XCTAssertEqual(formatTimestampAsHMS(60), "1:00")
        XCTAssertEqual(formatTimestampAsHMS(61), "1:01")
        XCTAssertEqual(formatTimestampAsHMS(3599), "59:59")
        XCTAssertEqual(formatTimestampAsHMS(3600), "1:00:00")
        XCTAssertEqual(formatTimestampAsHMS(3601), "1:00:01")
        XCTAssertEqual(formatTimestampAsHMS(215999), "59:59:59")
        XCTAssertEqual(formatTimestampAsHMS(216000), "60:00:00")
        XCTAssertEqual(formatTimestampAsHMS(216001), "60:00:01")

        XCTAssertEqual(formatTimestampAsHMS(-0), "0:00")
        XCTAssertEqual(formatTimestampAsHMS(-1), "-0:01")
        XCTAssertEqual(formatTimestampAsHMS(-2), "-0:02")
        XCTAssertEqual(formatTimestampAsHMS(-59), "-0:59")
        XCTAssertEqual(formatTimestampAsHMS(-60), "-1:00")
        XCTAssertEqual(formatTimestampAsHMS(-61), "-1:01")
        XCTAssertEqual(formatTimestampAsHMS(-3599), "-59:59")
        XCTAssertEqual(formatTimestampAsHMS(-3600), "-1:00:00")
        XCTAssertEqual(formatTimestampAsHMS(-3601), "-1:00:01")
        XCTAssertEqual(formatTimestampAsHMS(-215999), "-59:59:59")
        XCTAssertEqual(formatTimestampAsHMS(-216000), "-60:00:00")
        XCTAssertEqual(formatTimestampAsHMS(-216001), "-60:00:01")
    }

    func testFormattingDecimalTimestamps() {
        XCTAssertEqual(formatTimestampAsHMS(0.1), "0:00")
        XCTAssertEqual(formatTimestampAsHMS(1.2), "0:01")
        XCTAssertEqual(formatTimestampAsHMS(2.3), "0:02")
        XCTAssertEqual(formatTimestampAsHMS(59.4), "0:59")
        XCTAssertEqual(formatTimestampAsHMS(60.5), "1:00")
        XCTAssertEqual(formatTimestampAsHMS(61.6), "1:01")
        XCTAssertEqual(formatTimestampAsHMS(3599.7), "59:59")
        XCTAssertEqual(formatTimestampAsHMS(3600.8), "1:00:00")
        XCTAssertEqual(formatTimestampAsHMS(3601.9), "1:00:01")

        XCTAssertEqual(formatTimestampAsHMS(-0.1), "-0:00")
        XCTAssertEqual(formatTimestampAsHMS(-1.2), "-0:01")
        XCTAssertEqual(formatTimestampAsHMS(-2.3), "-0:02")
        XCTAssertEqual(formatTimestampAsHMS(-59.4), "-0:59")
        XCTAssertEqual(formatTimestampAsHMS(-60.5), "-1:00")
        XCTAssertEqual(formatTimestampAsHMS(-61.6), "-1:01")
        XCTAssertEqual(formatTimestampAsHMS(-3599.7), "-59:59")
        XCTAssertEqual(formatTimestampAsHMS(-3600.8), "-1:00:00")
        XCTAssertEqual(formatTimestampAsHMS(-3601.9), "-1:00:01")

        XCTAssertEqual(formatTimestampAsHMS(1000.999999), "16:40")
        XCTAssertEqual(formatTimestampAsHMS(1001.000001), "16:41")

        for _ in 0..<10 {
            let timestamp = Double.random(in: -99999999...99999999)
            let truncatedTimestamp = floor(timestamp) + (timestamp < 0 ? 1 : 0)
            XCTAssertEqual(formatTimestampAsHMS(timestamp), formatTimestampAsHMS(truncatedTimestamp), "Formatting of randomly generated timestamp \(timestamp) doesn't match truncated version")
        }
    }

    func testArrayRemoveAtIndexes() {
        var arr = [1, 2, 3, 4, 5]
        arr.remove(at: IndexSet())
        XCTAssertEqual(arr, [1, 2, 3, 4, 5])
        arr.remove(at: IndexSet(integer: 1))
        XCTAssertEqual(arr, [1, 3, 4, 5])
        arr.remove(at: IndexSet(arrayLiteral: 1, 3))
        XCTAssertEqual(arr, [1, 4])
        arr.remove(at: IndexSet(arrayLiteral: 0, 1))
        XCTAssertEqual(arr, [])
        arr.remove(at: IndexSet())
        XCTAssertEqual(arr, [])
    }

}
