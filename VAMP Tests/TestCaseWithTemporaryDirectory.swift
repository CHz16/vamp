//
//  TestCaseWithTemporaryDirectory.swift
//  VAMP Tests
//
//  Created by 'Ili Butterfield on 2/11/20.
//  Copyright © 2020 'Ili Butterfield. All rights reserved.
//

import XCTest

class TestCaseWithTemporaryDirectory: XCTestCase {

    var bundle: Bundle!
    var temporaryDirectoryURL: URL!

    override func setUp() {
        if bundle == nil {
            bundle = Bundle(for: type(of: self))
        }

        temporaryDirectoryURL = URL(fileURLWithPath: NSTemporaryDirectory()).appendingPathComponent(bundle.bundleIdentifier!, isDirectory: true)
        try? FileManager.default.removeItem(at: temporaryDirectoryURL) // directory might exist if a test crashed or something
        try! FileManager.default.createDirectory(at: temporaryDirectoryURL, withIntermediateDirectories: true)
    }

    override func tearDown() {
        try! FileManager.default.removeItem(at: temporaryDirectoryURL)
        temporaryDirectoryURL = nil
    }

    func makeEmptyTemporaryFile(withName name: String) -> URL {
        let url = temporaryDirectoryURL.appendingPathComponent(name)
        _ = FileManager.default.createFile(atPath: url.path, contents: nil, attributes: nil)
        return url
    }

}
