//
//  MIDIFileInfo_Tests.swift
//  VAMP Tests
//
//  Created by 'Ili Butterfield on 2/9/20.
//  Copyright © 2020 'Ili Butterfield. All rights reserved.
//

import XCTest

class MIDIFileInfo_Tests: TestCaseWithTemporaryDirectory {

    func testLoadingUntitledMIDI() throws {
        let fileInfo = try MIDIFileInfo(url: bundle.url(forResource: "valid untitled", withExtension: "mid")!)
        XCTAssertNil(fileInfo.title, "Title of untitled MIDI is not nil")
        XCTAssertEqual(fileInfo.longTitle, "valid untitled.mid", "Long title of untitled MIDI is not just the filename")
    }

    func testLoadingTitledMIDI() throws {
        let fileInfo = try MIDIFileInfo(url: bundle.url(forResource: "valid titled", withExtension: "mid")!)
        XCTAssertNotNil(fileInfo.title, "Title of titled MIDI is nil")
        XCTAssertEqual(fileInfo.title, "test tagged", "Title of titled MIDI is incorrect")
        XCTAssertEqual(fileInfo.longTitle, "valid titled.mid [test tagged]", "Long title of titled MIDI is not the filename + the title")
    }

    func testLoadingEmptyMIDI() throws {
        do {
            let url = makeEmptyTemporaryFile(withName: "invalid empty.mid")
            _ = try MIDIFileInfo(url: url)
            XCTFail("No exception thrown when loading an empty MIDI file")
        } catch MIDIFileInfoError.couldntLoadFile(let status) where status == -10871 {
            // pass the test if the error code is kAudioUnitErr_InvalidFile
        }
    }

    func testLoadingRandomTextMIDI() throws {
        do {
            _ = try MIDIFileInfo(url: bundle.url(forResource: "invalid text", withExtension: "mid")!)
            XCTFail("No exception thrown when loading a MIDI file with random text in it")
        } catch MIDIFileInfoError.couldntLoadFile(let status) where status == -10871 {
            // pass the test if the error code is kAudioUnitErr_InvalidFile
        }
    }

    func testLoadingCorruptMIDI() throws {
        _ = try MIDIFileInfo(url: bundle.url(forResource: "invalid corrupt", withExtension: "mid")!)
        // This corrupt MIDI should "load" correctly, but the track data is invalid so actually playing it won't work
    }

    func testRefreshingStaticMIDI() throws {
        let fileInfo = try MIDIFileInfo(url: bundle.url(forResource: "valid titled", withExtension: "mid")!)
        let refreshedFileInfo = try fileInfo.refresh()
        XCTAssertEqual(fileInfo.title, refreshedFileInfo.title, "Title of refreshed MIDI is incorrect")
        XCTAssertEqual(fileInfo.uuid, refreshedFileInfo.uuid, "UUID of refreshed MIDI is incorrect")
    }

    func testRefreshingMovedMIDI() throws {
        let firstURL = temporaryDirectoryURL.appendingPathComponent("first.mid")
        try FileManager.default.copyItem(at: bundle.url(forResource: "valid titled", withExtension: "mid")!, to: firstURL)
        let fileInfo = try MIDIFileInfo(url: firstURL)

        let secondURL = temporaryDirectoryURL.appendingPathComponent("second.mid")
        try FileManager.default.moveItem(at: firstURL, to: secondURL)
        let refreshedFileInfo = try fileInfo.refresh()
        XCTAssertEqual(refreshedFileInfo.filename, "second.mid", "Filename of refreshed MIDI is incorrect")
        XCTAssertEqual(fileInfo.title, refreshedFileInfo.title, "Title of refreshed MIDI is incorrect")
        XCTAssertEqual(fileInfo.uuid, refreshedFileInfo.uuid, "UUID of refreshed MIDI is incorrect")
    }

    func testRefreshingDeletedMIDI() throws {
        do {
            let url = temporaryDirectoryURL.appendingPathComponent("first.mid")
            try FileManager.default.copyItem(at: bundle.url(forResource: "valid titled", withExtension: "mid")!, to: url)
            let fileInfo = try MIDIFileInfo(url: url)

            try FileManager.default.removeItem(at: url)
            _ = try fileInfo.refresh()
            XCTFail("No exception thrown while refreshing a deleted file")
        } catch MIDIFileInfoError.couldntLoadFile(let status) where status == -1 {
            // pass the test if the error code is -1
        }
    }

    func testRefreshingCorruptedMIDI() throws {
        do {
            let url = temporaryDirectoryURL.appendingPathComponent("first.mid")
            try FileManager.default.copyItem(at: bundle.url(forResource: "valid titled", withExtension: "mid")!, to: url)
            let fileInfo = try MIDIFileInfo(url: url)

            try Data().write(to: url)
            _ = try fileInfo.refresh()
            XCTFail("No exception thrown while refreshing a corrupted file")
        } catch MIDIFileInfoError.couldntLoadFile(let status) where status == -10871 {
            // pass the test if the error code is kAudioUnitErr_InvalidFile
        }
    }

    func testCollectingTrackInfo() throws {
        let fileInfo = try MIDIFileInfo(url: bundle.url(forResource: "meta", withExtension: "mid")!)
        let trackInfos = fileInfo.trackInfos
        XCTAssertEqual(trackInfos.count, 3, "Number of tracks in MIDI isn't 3")

        XCTAssertEqual(trackInfos[0].text, "here is some text\na second, longer line of text in a separate event", "Text of track 0 is incorrect")
        XCTAssertEqual(trackInfos[0].copyright, "copyright line 1\ncopyright line 2\ncopyright line 3", "Copyright of track 0 is incorrect")
        XCTAssertEqual(trackInfos[0].trackName, "meta", "Track name of track 0 is incorrect")
        XCTAssertNil(trackInfos[0].instrumentName, "Instrument name of track 0 is not nil")
        XCTAssertNil(trackInfos[0].lyrics, "Lyrics of track 0 are not nil")
        XCTAssertNil(trackInfos[0].markers, "Markers of track 0 are not nil")
        XCTAssertNil(trackInfos[0].cuePoints, "Cue points of track 0 are not nil")
        XCTAssertNil(trackInfos[0].programName, "Program name of track 0 is not nil")
        XCTAssertNil(trackInfos[0].deviceName, "Device name of track 0 is not nil")

        XCTAssertEqual(trackInfos[1].text, "(this is where I put the telephone noises)", "Text of track 1 is incorrect")
        XCTAssertNil(trackInfos[1].copyright, "Copyright of track 1 is not nil")
        XCTAssertEqual(trackInfos[1].trackName, "ringy rang", "Track name of track 1 is incorrect")
        XCTAssertEqual(trackInfos[1].instrumentName, "Telephone", "Instrument name of track 1 is incorrect")
        XCTAssertNil(trackInfos[1].lyrics, "Lyrics of track 1 are not nil")
        XCTAssertEqual(trackInfos[1].markers, "start of track marker\nend of track marker", "Markers of track 1 are incorrect")
        XCTAssertEqual(trackInfos[1].cuePoints, "loop start\nloop end", "Cue points of track 1 are incorrect")
        XCTAssertNil(trackInfos[1].programName, "Program name of track 1 is not nil")
        XCTAssertEqual(trackInfos[1].deviceName, "MIDI Out 1", "Device name of track 1 is incorrect")

        XCTAssertNil(trackInfos[2].text, "Text of track 2 is not nil")
        XCTAssertNil(trackInfos[2].copyright, "Copyright of track 2 is not nil")
        XCTAssertEqual(trackInfos[2].trackName, "bwah", "Track name of track 2 is incorrect")
        XCTAssertEqual(trackInfos[2].instrumentName, "Soundtrack", "Instrument name of track 2 is not nil")
        XCTAssertEqual(trackInfos[2].lyrics, "don't mind me just making up some lyrics.", "Lyrics of track 2 are incorrect")
        XCTAssertNil(trackInfos[2].markers, "Markers of track 2 are not nil")
        XCTAssertNil(trackInfos[2].cuePoints, "Cue points of track 2 are not nil")
        XCTAssertEqual(trackInfos[2].programName, "Program Name, which is different from Instrument Name", "Program name of track 2 is incorrect")
        XCTAssertEqual(trackInfos[2].deviceName, "MIDI Out 2", "Device name of track 2 is incorrect")
    }

}
