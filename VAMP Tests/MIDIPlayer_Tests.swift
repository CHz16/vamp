//
//  MIDIPlayer_Tests.swift
//  VAMP Tests
//
//  Created by 'Ili Butterfield on 2/11/20.
//  Copyright © 2020 'Ili Butterfield. All rights reserved.
//

import XCTest

// These tests just check that the functions of MIDIPlayer don't crash or anything; they don't verify the audio output is what we're expecting.
class MIDIPlayer_Tests: TestCaseWithTemporaryDirectory {

    func testCreatingPlayer() throws {
        _ = try MIDIPlayer(url: bundle.url(forResource: "valid titled", withExtension: "mid")!)
    }

    func testCreatingPlayerWithEmptyMIDI() throws {
        do {
            let url = makeEmptyTemporaryFile(withName: "invalid empty.mid")
            _ = try MIDIPlayer(url: url)
            XCTFail("No exception thrown when creating a player with an empty MIDI")
        } catch MIDIPlayerError.couldntLoadMIDIFile(let status) where status == -10871 {
            // pass the test if the error code is kAudioUnitErr_InvalidFile
        }
    }

    func testCreatingPlayerWithRandomTextMIDI() throws {
        do {
            _ = try MIDIPlayer(url: bundle.url(forResource: "invalid text", withExtension: "mid")!)
            XCTFail("No exception thrown when creating a player with a MIDI with random text in it")
        } catch MIDIPlayerError.couldntLoadMIDIFile(let status) where status == -10871 {
            // pass the test if the error code is kAudioUnitErr_InvalidFile
        }
    }

    func testStartingAndStoppingPlayer() throws {
        let player = try MIDIPlayer(url: bundle.url(forResource: "valid titled", withExtension: "mid")!)
        try player.start()
        try player.stop()
    }

    func testStartingPlayerWithCorruptMIDI() throws {
        do {
            let player = try MIDIPlayer(url: bundle.url(forResource: "invalid corrupt", withExtension: "mid")!)
            try player.start()
            XCTFail("No exception thrown when starting a player with a corrupt MIDI")
        } catch MIDIPlayerError.couldntStartMusicPlayer(let status) where status == -10852 {
            // pass the test if the error code is kAudioToolboxErr_InvalidPlayerState
        }
    }

    func testSettingAndGettingPlayerTime() throws {
        let player = try MIDIPlayer(url: bundle.url(forResource: "valid titled", withExtension: "mid")!)
        XCTAssertEqual(try player.getTime(), 0, accuracy: 0.0001, "Player time not 0 on creation")
        try player.setTime(5)
        XCTAssertEqual(try player.getTime(), 5, accuracy: 0.0001, "Player time not 5 after setting it to 5")
        try player.setTime(0)
        XCTAssertEqual(try player.getTime(), 0, accuracy: 0.0001, "Player time not 0 after setting it back to 0")
        try player.setTime(55555)
        XCTAssertEqual(try player.getTime(), 55555, accuracy: 0.0001, "Player time not 55555 after setting it to 55555")
        try player.setTime(-55555)
        XCTAssertEqual(try player.getTime(), 0, accuracy: 0.0001, "Player time not 0 after setting it to -55555")
    }

    func testSettingPlayerVolume() throws {
        let player = try MIDIPlayer(url: bundle.url(forResource: "valid titled", withExtension: "mid")!)
        try player.setVolume(0.5)
        try player.setVolume(0)
        try player.setVolume(1)
        try player.setVolume(123456.789)
        try player.setVolume(-123456.789)
        try player.start()
    }

    func testSettingSystemSoundFont() throws {
        let player = try MIDIPlayer(url: bundle.url(forResource: "valid titled", withExtension: "mid")!)
        let soundFont = try SoundFont()
        try player.setSoundFont(url: soundFont.url)
        try player.start()
    }

    func testSettingCustomSoundFont() throws {
        let player = try MIDIPlayer(url: bundle.url(forResource: "valid titled", withExtension: "mid")!)
        try player.setSoundFont(url: bundle.url(forResource: "GeneralUser GS 1.35", withExtension: "sf2")!)
        try player.start()
    }

    func testSettingCustomSoundFontWhilePlaying() throws {
        let player = try MIDIPlayer(url: bundle.url(forResource: "valid titled", withExtension: "mid")!)
        try player.start()
        let waitExpectation = expectation(description: "waiting")
        let result = XCTWaiter.wait(for: [waitExpectation], timeout: 0.05)
        switch result {
        case .timedOut:
            try player.setSoundFont(url: bundle.url(forResource: "GeneralUser GS 1.35", withExtension: "sf2")!)
        default:
            XCTFail("Something weird happened while trying to wait and we got a result of \(result) instead")
        }
    }

    func testSettingEmptySoundFont() throws {
        do {
            let player = try MIDIPlayer(url: bundle.url(forResource: "valid titled", withExtension: "mid")!)
            let emptySoundFontURL = makeEmptyTemporaryFile(withName: "invalid empty.sf2")
            try player.setSoundFont(url: emptySoundFontURL)
            XCTFail("No exception thrown when giving an empty soundfont to a player")
        } catch MIDIPlayerError.couldntLoadSoundFont(let status) where status == -10871 {
            // pass the test if the error code is kAudioUnitErr_InvalidFile
        }
    }

    func testSettingRandomTextSoundFont() throws {
        do {
            let player = try MIDIPlayer(url: bundle.url(forResource: "valid titled", withExtension: "mid")!)
            try player.setSoundFont(url: bundle.url(forResource: "invalid text", withExtension: "sf2")!)
            XCTFail("No exception thrown when giving a soundfont with random text in it to a player")
        } catch MIDIPlayerError.couldntLoadSoundFont(let status) where status == -10871 {
            // pass the test if the error code is kAudioUnitErr_InvalidFile
        }
    }

}


// These tests actually verify that the audio output is what we're expecting by using the MIDIExporter class and comparing output to reference files.
// The reference files were all generated with this code to begin with, so they can test if any future code modifications change the rendered output, but they don't prove that the code was outputting "correct" audio to begin with lmao
class MIDIPlayer_AudioTests: TestCaseWithTemporaryDirectory {

    func testAudioOutput() throws {
        let tests = [
            (midi: "valid titled", wav: "default test", duration: 6.0),
            (midi: "meta", wav: "meta test", duration: 8.0)
        ]

        for (midi, wav, duration) in tests {
            let fileInfo = try MIDIFileInfo(url: bundle.url(forResource: midi, withExtension: "mid")!)
            let soundFont = try SoundFont()
            let exporter = try MIDIExporter(file: fileInfo, soundFont: soundFont)

            let url = temporaryDirectoryURL.appendingPathComponent("test.wav")
            try exporter.export(to: url, duration: duration)
            XCTAssertTrue(FileManager.default.contentsEqual(atPath: url.path, andPath: bundle.path(forResource: wav, ofType: "wav")!), "Output of test MIDI \(midi) using system soundfont doesn't match reference render")
        }
    }

    func testUsingCustomSoundFont() throws {
        let fileInfo = try MIDIFileInfo(url: bundle.url(forResource: "valid titled", withExtension: "mid")!)
        let soundFont = try SoundFont(url: bundle.url(forResource: "GeneralUser GS 1.35", withExtension: "sf2")!)
        let exporter = try MIDIExporter(file: fileInfo, soundFont: soundFont)

        let url = temporaryDirectoryURL.appendingPathComponent("test.wav")
        try exporter.export(to: url, duration: 6)
        XCTAssertTrue(FileManager.default.contentsEqual(atPath: url.path, andPath: bundle.path(forResource: "soundfont test", ofType: "wav")!), "Output of test MIDI using custom soundfont doesn't match reference render")
    }

    func testChangingSongPosition() throws {
        let fileInfo = try MIDIFileInfo(url: bundle.url(forResource: "valid titled", withExtension: "mid")!)
        let soundFont = try SoundFont()
        let exporter = try MIDIExporter(file: fileInfo, soundFont: soundFont)

        let url = temporaryDirectoryURL.appendingPathComponent("test.wav")
        try exporter.export(to: url, duration: 5.25, startAt: 0.75)
        XCTAssertTrue(FileManager.default.contentsEqual(atPath: url.path, andPath: bundle.path(forResource: "cut test", ofType: "wav")!), "Output of test MIDI starting at 0.75 seconds doesn't match reference render")
    }

    func testChangingVolume() throws {
        let fileInfo = try MIDIFileInfo(url: bundle.url(forResource: "valid titled", withExtension: "mid")!)
        let soundFont = try SoundFont()
        let exporter = try MIDIExporter(file: fileInfo, soundFont: soundFont)
        try exporter.player.setVolume(0.5)

        let url = temporaryDirectoryURL.appendingPathComponent("test.wav")
        try exporter.export(to: url, duration: 6)
        XCTAssertTrue(FileManager.default.contentsEqual(atPath: url.path, andPath: bundle.path(forResource: "volume test", ofType: "wav")!), "Output of test MIDI at half volume doesn't match reference render")
    }

}
