//
//  SoundFont_Tests.swift
//  VAMP Tests
//
//  Created by 'Ili Butterfield on 2/10/20.
//  Copyright © 2020 'Ili Butterfield. All rights reserved.
//

import XCTest

class SoundFont_Tests: TestCaseWithTemporaryDirectory {

    func testLoadingSystemSoundFont() throws {
        let soundFont = try SoundFont()
        XCTAssertTrue(soundFont.isSystemSoundFont, "Soundfont not flagged as system soundfont")
        XCTAssertEqual(soundFont.name, NSLocalizedString("systemSoundFontDefaultName", comment: "default system soundfont name"), "System soundfont name doesn't match localized name")
    }

    func testLoadingCustomSoundFont() throws {
        let url = bundle.url(forResource: "GeneralUser GS 1.35", withExtension: "sf2")!
        let soundFont = try SoundFont(url: url)
        XCTAssertFalse(soundFont.isSystemSoundFont, "Soundfont flagged as system soundfont")
        XCTAssertEqual(soundFont.filename, url.lastPathComponent, "Soundfont filename attribute doesn't match filename from URL")
        XCTAssertEqual(soundFont.name, url.lastPathComponent, "Soundfont default assigned name doesn't match filename from URL")
    }

    func testLoadingEmptySoundFont() throws {
        do {
            let url = makeEmptyTemporaryFile(withName: "invalid empty.sf2")
            _ = try SoundFont(url: url)
            XCTFail("No exception thrown when loading an empty soundfont")
        } catch SoundFontError.couldntLoadSoundFont(let status) where status == -10871 {
            // pass the test if the error code is kAudioUnitErr_InvalidFile
        }
    }

    func testLoadingRandomTextSoundFont() throws {
        do {
            let url = bundle.url(forResource: "invalid text", withExtension: "sf2")!
            _ = try SoundFont(url: url)
            XCTFail("No exception thrown when loading a soundfont with random text in it")
        } catch SoundFontError.couldntLoadSoundFont(let status) where status == -10871 {
            // pass the test if the error code is kAudioUnitErr_InvalidFile
        }
    }

    func testRefreshingSystemSoundFont() throws {
        var soundFont = try SoundFont()
        soundFont.name = "alternate title"
        let refreshedSoundFont = try soundFont.refresh()
        XCTAssertTrue(refreshedSoundFont.isSystemSoundFont, "Refreshed soundfont not flagged as system soundfont")
        XCTAssertEqual(soundFont.name, refreshedSoundFont.name, "Name of refreshed soundfont is incorrect")
        XCTAssertEqual(soundFont.uuid, refreshedSoundFont.uuid, "UUID of refreshed soundfont is incorrect")
    }

    func testRefreshingStaticSoundFont() throws {
        var soundFont = try SoundFont(url: bundle.url(forResource: "GeneralUser GS 1.35", withExtension: "sf2")!)
        soundFont.name = "alternate title"
        let refreshedSoundFont = try soundFont.refresh()
        XCTAssertFalse(refreshedSoundFont.isSystemSoundFont, "Refreshed soundfont flagged as system soundfont")
        XCTAssertEqual(soundFont.name, refreshedSoundFont.name, "Name of refreshed soundfont is incorrect")
        XCTAssertEqual(soundFont.uuid, refreshedSoundFont.uuid, "UUID of refreshed soundfont is incorrect")
    }

    func testRefreshingMovedSoundFont() throws {
        let firstURL = temporaryDirectoryURL.appendingPathComponent("first.sf2")
        try FileManager.default.copyItem(at: bundle.url(forResource: "GeneralUser GS 1.35", withExtension: "sf2")!, to: firstURL)
        var soundFont = try SoundFont(url: firstURL)
        soundFont.name = "alternate title"

        let secondURL = temporaryDirectoryURL.appendingPathComponent("second.sf2")
        try FileManager.default.moveItem(at: firstURL, to: secondURL)
        let refreshedSoundFont = try soundFont.refresh()
        XCTAssertEqual(refreshedSoundFont.filename, "second.sf2", "Filename of refreshed soundfont is incorrect")
        XCTAssertFalse(refreshedSoundFont.isSystemSoundFont, "Refreshed soundfont flagged as system soundfont")
        XCTAssertEqual(soundFont.name, refreshedSoundFont.name, "Name of refreshed soundfont is incorrect")
        XCTAssertEqual(soundFont.uuid, refreshedSoundFont.uuid, "UUID of refreshed soundfont is incorrect")
    }

    func testRefreshingDeletedSoundFont() throws {
        do {
            let url = temporaryDirectoryURL.appendingPathComponent("first.sf2")
            try FileManager.default.copyItem(at: bundle.url(forResource: "GeneralUser GS 1.35", withExtension: "sf2")!, to: url)
            let soundFont = try SoundFont(url: url)

            try FileManager.default.removeItem(at: url)
            _ = try soundFont.refresh()
            XCTFail("No exception thrown while refreshing a deleted soundfont")
        } catch SoundFontError.couldntLoadSoundFont(let status) where status == -43 {
            // pass the test if the error code is kAudioFileFileNotFoundError
        }
    }

    func testRefreshingCorruptedSoundFont() throws {
        do {
            let url = temporaryDirectoryURL.appendingPathComponent("first.sf2")
            try FileManager.default.copyItem(at: bundle.url(forResource: "GeneralUser GS 1.35", withExtension: "sf2")!, to: url)
            let soundFont = try SoundFont(url: url)

            try Data().write(to: url)
            _ = try soundFont.refresh()
            XCTFail("No exception thrown while refreshing a corrupted soundfont")
        } catch SoundFontError.couldntLoadSoundFont(let status) where status == -10871 {
            // pass the test if the error code is kAudioUnitErr_InvalidFile
        }
    }

}
