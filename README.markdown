VAMP
====

A vaguely accurate MIDI player for macOS (10.14 and up)

Current version download link: [1.1](https://bitbucket.org/CHz16/vamp/downloads/VAMP_1.1.zip). This program is unsigned, so you'll have to tell the operating system specially to run it: https://support.apple.com/guide/mac-help/open-a-mac-app-from-an-unidentified-developer-mh40616/mac

![VAMP screenshot](screenshot.png)

This is just a small project I made after finding out that the Mac audio libraries make it real easy to slap one of these together. All I really wanted was a 64-bit MIDI player with the ability to swap soundfonts on the fly and export to WAVE quickly, both of which take a slightly annoying amount of work in VLC.

Everything here except `VAMP Tests/SoundFonts/GeneralUser GS 1.35.sf2` and what's in `VAMP Tests/GeneralUser GS 1.35/` is by me and released under MIT (see LICENSE.txt). The code is extremely based on [Apple's PlaySequence sample project](https://developer.apple.com/library/archive/samplecode/PlaySequence/Introduction/Intro.html#//apple_ref/doc/uid/DTS40008652).

`VAMP Tests/SoundFonts/GeneralUser GS 1.35.sf2` is by [S. Christian Collins](http://www.schristiancollins.com/generaluser.php) and is released under the terms in `VAMP Tests/GeneralUser GS 1.35/GeneralUser GS 1.35.license.txt`. The soundfont is just used for unit testing purposes and isn't included with the application itself.
