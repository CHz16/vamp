//
//  TrackingSlider.swift
//  VAMP
//
//  Created by 'Ili Butterfield on 1/21/20.
//  Copyright © 2020 'Ili Butterfield. All rights reserved.
//

// This is just a simple subclass of NSSlider with an extra read-only property indicating whether the slider is currently tracking a drag event.

import AppKit
import Foundation


class TrackingSlider: NSSlider {
    fileprivate(set) var isTracking = false
}

class TrackingSliderCell: NSSliderCell {

    override func startTracking(at startPoint: NSPoint, in controlView: NSView) -> Bool {
        (controlView as! TrackingSlider).isTracking = true
        return super.startTracking(at: startPoint, in: controlView)
    }

    override func stopTracking(last lastPoint: NSPoint, current stopPoint: NSPoint, in controlView: NSView, mouseIsUp flag: Bool) {
        (controlView as! TrackingSlider).isTracking = false
        super.stopTracking(last: lastPoint, current: stopPoint, in: controlView, mouseIsUp: flag)
    }

}
