//
//  RandomGarbage.swift
//  VAMP
//
//  Created by 'Ili Butterfield on 1/17/20.
//  Copyright © 2020 'Ili Butterfield. All rights reserved.
//

import AppKit
import Foundation


func formatTimestampAsHMS(_ timestamp: Double) -> String {
    let roundedTimestamp = Int(abs(timestamp))
    let hours = roundedTimestamp / 3600
    let minutes = (roundedTimestamp % 3600) / 60
    let seconds = roundedTimestamp % 60

    let s: String
    if hours > 0 {
        s =  String(format: "%d:%02d:%02d", hours, minutes, seconds)
    } else {
        s = String(format: "%d:%02d", minutes, seconds)
    }
    return (timestamp < 0) ? "-\(s)" : s
}


extension Array {

    mutating func remove(at indexes: IndexSet) {
        // It would be real nice if Array had remove(at: IndexSet) like NSMutableArray does, but it doesn't, so I hope you enjoy some crime
        let tempArray = NSMutableArray(array: self)
        tempArray.removeObjects(at: indexes)
        self = tempArray as! [Self.Element]
    }

}


extension NSViewController {

    func showErrorInCurrentWindow(key: String, messageArguments: [CVarArg] = [], informationArguments: [CVarArg] = [], error: Error? = nil, terminateAfter: Bool = false) {
        // String.localizedStringWithFormat doesn't accept [CVarArg], only variadic CVarArg..., and Swift doesn't have splatting, so instead we'll interpolate the format string by using String(format:locale:arguments:)
        // According to the docs, this should be equivalent
        let messageText = String(format: NSLocalizedString(key + ".message", comment: ""), locale: Locale.current, arguments: messageArguments)
        let informativeText = String(format: NSLocalizedString(key + ".information", comment: ""), locale: Locale.current, arguments: informationArguments)
        let errorText = (error != nil) ? "\n\n\(error!)" : ""

        let alert = NSAlert()
        alert.icon = NSImage(named: NSImage.cautionName)
        alert.messageText = messageText
        alert.informativeText = "\(informativeText)\(errorText)"
        alert.beginSheetModal(for: view.window!) { response in
            if terminateAfter {
                NSApp.terminate(self)
            }
        }
    }

}
