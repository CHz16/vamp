//
//  MIDIFileInfo.swift
//  VAMP
//
//  Created by 'Ili Butterfield on 1/17/20.
//  Copyright © 2020 'Ili Butterfield. All rights reserved.
//

import AudioToolbox
import Foundation


enum MIDIFileInfoError: Error {
    case couldntCreateSequence(status: OSStatus)
    case couldntLoadFile(status: OSStatus)
    case couldntGetTrackCount(status: OSStatus)
    case couldntGetTrack(status: OSStatus)
    case couldntGetTrackLength(status: OSStatus)
    case couldntMakeEventIterator(status: OSStatus)
    case couldntDisposeEventIterator(status: OSStatus)
    case couldntCheckEvent(status: OSStatus)
    case couldntGetEvent(status: OSStatus)
    case couldntAdvanceEventIterator(status: OSStatus)
    case couldntCalculateDuration(status: OSStatus)
    case couldntDisposeSequence(status: OSStatus)
}

struct TrackInfo { let text, copyright, trackName, instrumentName, lyrics, markers, cuePoints, programName, deviceName: String? }


struct MIDIFileInfo {

    let url: URL
    let bookmarkData: Data?
    let filename: String
    let title: String?
    let duration: Double
    let uuid: UUID
    let trackInfos: [TrackInfo]

    var longTitle: String {
        if let title = title {
            return "\(filename) [\(title)]"
        } else {
            return filename
        }
    }

    init(url inURL: URL) throws {
        try self.init(url: inURL, uuid: UUID())
    }

    private init(url inURL: URL, uuid inUUID: UUID) throws {
        url = inURL
        bookmarkData = try? url.bookmarkData()
        filename = url.lastPathComponent
        uuid = inUUID

        // Load the MIDI file
        var sequence: MusicSequence!
        var result = NewMusicSequence(&sequence)
        if result != noErr {
            throw MIDIFileInfoError.couldntCreateSequence(status: result)
        }
        result = MusicSequenceFileLoad(
            sequence, // inSequence
            url as CFURL, // inFileRef
            MusicSequenceFileTypeID.midiType, // inFileTypeHint
            [] // inFlags
        )
        if result != noErr {
            throw MIDIFileInfoError.couldntLoadFile(status: result)
        }

        // Get the information dictionary about the sequence and check if the MIDI has a title set
        // The docs say this can return NULL on failure in Obj-C, but in Swift the return type is CFDictionary so it seems like we always get something?
        let infoDictionary = MusicSequenceGetInfoDictionary(sequence) as Dictionary
        if let titleTag = infoDictionary[kAFInfoDictionary_Title as NSObject] as? String, !titleTag.isEmpty {
            title = titleTag
        } else {
            title = nil
        }

        // Get the number of tracks
        var numberOfTracks: UInt32 = 0
        result = MusicSequenceGetTrackCount(sequence, &numberOfTracks)
        if result != noErr {
            throw MIDIFileInfoError.couldntGetTrackCount(status: result)
        }

        // Find the length of the MIDI in beats by iterating through each track and finding the maximum length of a track
        // Also read in all of a track's textual meta events for display
        var sequenceLength: MusicTimeStamp = 0
        var trackInfos: [TrackInfo] = []
        for i in 0..<numberOfTracks {
            var stringBuckets: [UInt8: [String]] = [:]

            // Get the track object
            var track: MusicTrack!
            result = MusicSequenceGetIndTrack(sequence, i, &track)
            if result != noErr {
                throw MIDIFileInfoError.couldntGetTrack(status: result)
            }

            // Get the beat length
            var trackLength: MusicTimeStamp = 0
            var mtpSize = UInt32(MemoryLayout<MusicTimeStamp>.stride)
            result = MusicTrackGetProperty(
                track, // inTrack
                kSequenceTrackProperty_TrackLength, // inProperty
                &trackLength, // outData
                &mtpSize // ioLength
            )
            if result != noErr {
                throw MIDIFileInfoError.couldntGetTrackLength(status: result)
            }
            sequenceLength = max(sequenceLength, trackLength)

            // Create an iterator to go through all events in the track and then read them
            var eventIterator: MusicEventIterator!
            result = NewMusicEventIterator(track, &eventIterator)
            if result != noErr {
                throw MIDIFileInfoError.couldntMakeEventIterator(status: result)
            }

            while true {
                // Check if there's another event to read
                var hasCurrentEvent: DarwinBoolean = false
                result = MusicEventIteratorHasCurrentEvent(eventIterator, &hasCurrentEvent)
                if result != noErr {
                    throw MIDIFileInfoError.couldntCheckEvent(status: result)
                }
                if !hasCurrentEvent.boolValue {
                    break
                }

                // Get the event object
                var timestamp: MusicTimeStamp = 0 // We don't actually use this, but it's necessary for the call
                var eventType: MusicEventType = kMusicEventType_NULL
                var eventData: UnsafeRawPointer? = nil
                var eventDataSize: UInt32 = 0
                result = MusicEventIteratorGetEventInfo(
                    eventIterator, // inIterator
                    &timestamp, // outTimestamp
                    &eventType, // outEventType
                    &eventData, // outEventData
                    &eventDataSize // outEventDataSize
                )
                if result != noErr {
                    throw MIDIFileInfoError.couldntGetEvent(status: result)
                }

                // If we read a meta event, check if the meta event type is one we care about and, if so, read the data into a string
                // It's possible for there to be more than one event of the same type, so we'll collect them all and then concatenate them later
                // The eventData we get out of the event info struct is a blob, and we're expected to cast it to the specific type we care about based on the eventType we got at the same time (which is always MIDIMetaEvent, since that's all we care about)
                // Then, we need to decote the .data member of that struct. It's a variadic array declare in the original C API as a single-element array, which very inconveniently gets exposed to Swift as a single-element tuple. So, in order to get the array of bytes that that member actually is, we need to get a pointer to the start of that "tuple" and then read the number of bytes in event.dataLength
                // Shoutouts to AudioKit for figuring this out for me years ago
                // https://github.com/AudioKit/AudioKit/blob/ebf0460bf1b3fc8d3888d26d9aa926892294cd0c/Sources/AudioKit/Sequencing/Apple%20Sequencer/UnsafeMIDIMetaEventPointer.swift
                if eventType == kMusicEventType_Meta {
                    let data = UnsafePointer<MIDIMetaEvent>(eventData?.assumingMemoryBound(to: MIDIMetaEvent.self))
                    if let event = data?.pointee {
                        if event.metaEventType >= 1 && event.metaEventType <= 9 {
                            let offset = MemoryLayout<MIDIMetaEvent>.offset(of: \MIDIMetaEvent.data)!
                            let dataPointer = UnsafeRawPointer(data!).advanced(by: offset)
                            let nameData = Data(bytes: dataPointer, count: Int(event.dataLength))
                            if let s = String(bytes: nameData, encoding: .ascii) {
                                stringBuckets[event.metaEventType, default: []].append(s)
                            }
                        }
                    }
                }

                // Advance the iterator
                result = MusicEventIteratorNextEvent(eventIterator)
                if result != noErr {
                    throw MIDIFileInfoError.couldntAdvanceEventIterator(status: result)
                }
            }

            // Join up all the text meta events we found in the track
            // Event types & descriptions: https://www.mixagesoftware.com/en/midikit/help/HTML/meta_events.html
            trackInfos.append(TrackInfo(
                text: stringBuckets[1]?.joined(separator: "\n"),
                copyright: stringBuckets[2]?.joined(separator: "\n"),
                trackName: stringBuckets[3]?.joined(separator: "\n"),
                instrumentName: stringBuckets[4]?.joined(separator: "\n"),
                lyrics: stringBuckets[5]?.joined(),
                markers: stringBuckets[6]?.joined(separator: "\n"),
                cuePoints: stringBuckets[7]?.joined(separator: "\n"),
                programName: stringBuckets[8]?.joined(separator: "\n"),
                deviceName: stringBuckets[9]?.joined(separator: "\n")
            ))

            // Free the iterator after we're done
            result = DisposeMusicEventIterator(eventIterator)
            if result != noErr {
                throw MIDIFileInfoError.couldntDisposeEventIterator(status: result)
            }
        }
        self.trackInfos = trackInfos

        // Get the duration of the MIDI by converting the beats to seconds
        var seconds: Float64 = 0
        result = MusicSequenceGetSecondsForBeats(sequence, sequenceLength, &seconds)
        if result != noErr {
            throw MIDIFileInfoError.couldntCalculateDuration(status: result)
        }
        duration = seconds

        // Dispose of the sequence
        result = DisposeMusicSequence(sequence)
        if result != noErr {
            throw MIDIFileInfoError.couldntDisposeSequence(status: result)
        }
    }

    func refresh() throws -> MIDIFileInfo {
        let loadURL: URL
        if let bookmarkData = bookmarkData {
            do {
                var isStale = false
                loadURL = try URL.init(resolvingBookmarkData: bookmarkData, bookmarkDataIsStale: &isStale)
            } catch {
                loadURL = url
            }
        } else {
            loadURL = url
        }

        return try MIDIFileInfo(url: loadURL, uuid: uuid)
    }

}
