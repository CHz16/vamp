//
//  SoundFont.swift
//  VAMP
//
//  Created by 'Ili Butterfield on 1/23/20.
//  Copyright © 2020 'Ili Butterfield. All rights reserved.
//

import Foundation


enum SoundFontError: Error {
    case couldntFindSystemSoundFont
    case couldntLoadSoundFont(status: OSStatus)
}

struct SoundFont: Codable {

    var name: String
    let isSystemSoundFont: Bool
    let filename: String
    let url: URL
    let bookmarkData: Data?
    let uuid: UUID

    fileprivate static func systemSoundFontURL() throws -> URL {
        let potentialPaths = [
            "/System/Library/Components/CoreAudio.component/Contents/Resources/gs_instruments.dls"
        ]
        for path in potentialPaths {
            if FileManager.default.fileExists(atPath: path) {
                return URL(fileURLWithPath: path)
            }
        }
        throw SoundFontError.couldntFindSystemSoundFont
    }

    init() throws {
        // The ideal solution here would be to find the system soundfont automatically instead of hardcoding its path here, but I've tried reading that property and it returns a kAudioUnitErr_InvalidProperty error, even though setting it using the same property works okay.
        // This is issue #3
        try self.init(url: try SoundFont.systemSoundFontURL(), isSystemSoundFont: true, name: NSLocalizedString("systemSoundFontDefaultName", comment: "default system soundfont name"), uuid: UUID())
    }

    init(url inURL: URL) throws {
        try self.init(url: inURL, isSystemSoundFont: false, name: inURL.lastPathComponent, uuid: UUID())
    }

    private init(url inURL: URL, isSystemSoundFont inIsSystemSoundFont: Bool, name inName: String, uuid inUUID: UUID) throws {
        url = inURL
        bookmarkData = try? url.bookmarkData()
        filename = url.lastPathComponent
        name = inName
        isSystemSoundFont = inIsSystemSoundFont
        uuid = inUUID

        // Verify that the soundfont is good
        // We'll do it by just taking a test MIDI file and seeing if we can make a player out of it with the provided soundfont
        do {
            let midiPlayer = try MIDIPlayer(url: Bundle.main.url(forResource: "test", withExtension: "mid")!)
            try midiPlayer.setSoundFont(url: url)
        } catch MIDIPlayerError.couldntLoadSoundFont(let status) {
            // We'll re-wrap this error in a SoundFontError object to indicate that the error is from us
            throw SoundFontError.couldntLoadSoundFont(status: status)
        } catch {
            throw error
        }
    }

    func refresh() throws -> SoundFont {
        let loadURL: URL
        if isSystemSoundFont {
            loadURL = try SoundFont.systemSoundFontURL()
        } else {
            if let bookmarkData = bookmarkData {
                do {
                    var isStale = false
                    loadURL = try URL.init(resolvingBookmarkData: bookmarkData, bookmarkDataIsStale: &isStale)
                } catch {
                    loadURL = url
                }
            } else {
                loadURL = url
            }
        }

        return try SoundFont(url: loadURL, isSystemSoundFont: isSystemSoundFont, name: name, uuid: uuid)
    }

}
