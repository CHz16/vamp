//
//  MIDIPlayer.swift
//  VAMP
//
//  Created by 'Ili Butterfield on 1/15/20.
//  Copyright © 2020 'Ili Butterfield. All rights reserved.
//

import AudioToolbox
import Foundation


enum MIDIPlayerError: Error {
    case couldntCreateSequence(status: OSStatus)
    case couldntLoadMIDIFile(status: OSStatus)
    case couldntCreateGraph(status: OSStatus)
    case couldntOpenGraph(status: OSStatus)
    case couldntInitializeGraph(status: OSStatus)
    case couldntGetNodeCount(status: OSStatus)
    case couldntGetNode(status: OSStatus)
    case couldntGetNodeInfo(status: OSStatus)
    case couldntFindSampler
    case couldntFindCompressor
    case couldntFindMixer
    case couldntFindOutputNode
    case couldntDisconnectOutputNode(status: OSStatus)
    case couldntConnectMixer(status: OSStatus)
    case couldntConnectOutputNode(status: OSStatus)
    case couldntCreateMixer(status: OSStatus)
    case couldntCreateMusicPlayer(status: OSStatus)
    case couldntSetSequence(status: OSStatus)
    case couldntPrerollMusicPlayer(status: OSStatus)
    case couldntStartMusicPlayer(status: OSStatus)
    case couldntStopMusicPlayer(status: OSStatus)
    case couldntLoadSoundFont(status: OSStatus)
    case couldntSetVolume(status: OSStatus)
    case couldntGetTime(status: OSStatus)
    case couldntConvertBeatsToSeconds(status: OSStatus)
    case couldntSetTime(status: OSStatus)
    case couldntConvertSecondsToBeats(status: OSStatus)
}

class MIDIPlayer {

    var player: MusicPlayer!
    var sequence: MusicSequence!
    var graph: AUGraph!
    var sampler: AudioUnit!
    var mixer: AudioUnit!
    // we don't need to hold onto these nodes here beyond init, but they're used in MIDIExporter's extension to this
    var mixerNode, outputNode: AUNode!
    var output: AudioUnit!

    var playing = false

    init(url: URL) throws {
        // Load the MIDI file
        var result = NewMusicSequence(&sequence)
        if result != noErr {
            throw MIDIPlayerError.couldntCreateSequence(status: result)
        }
        result = MusicSequenceFileLoad(
            sequence, // inSequence
            url as CFURL, // inFileRef
            MusicSequenceFileTypeID.midiType, // inFileTypeHint
            [] // inFlags
        )
        if result != noErr {
            throw MIDIPlayerError.couldntLoadMIDIFile(status: result)
        }

        // Create the graph
        result = MusicSequenceGetAUGraph(sequence, &graph)
        if result != noErr {
            throw MIDIPlayerError.couldntCreateGraph(status: result)
        }

        // Create the mixer node
        var mixerDescription = AudioComponentDescription(
            componentType: kAudioUnitType_Mixer,
            componentSubType: kAudioUnitSubType_StereoMixer,
            componentManufacturer: kAudioUnitManufacturer_Apple,
            componentFlags: 0,
            componentFlagsMask: 0
        )
        mixerNode = AUNode()
        result = AUGraphAddNode(graph, &mixerDescription, &mixerNode)
        if result != noErr {
            throw MIDIPlayerError.couldntCreateMixer(status: result)
        }

        // Now we can open the graph
        result = AUGraphOpen(graph)
        if result != noErr {
            throw MIDIPlayerError.couldntOpenGraph(status: result)
        }

        // Identify the nodes in the graph and get their units
        // There's a DLSMusicDevice, AUDynamicsProcessor, AUMixer, and DefaultOutputUnit, and we need all of them for various connection and parameter purposes
        var nodeCount: UInt32 = 0
        result = AUGraphGetNodeCount(graph, &nodeCount)
        if result != noErr {
            throw MIDIPlayerError.couldntGetNodeCount(status: result)
        }

        var compressorNode: AUNode!
        for i in UInt32(0)..<nodeCount {
            var node = AUNode()
            result = AUGraphGetIndNode(graph, i, &node)
            if result != noErr {
                throw MIDIPlayerError.couldntGetNode(status: result)
            }

            var componentDescription = AudioComponentDescription()
            var audioUnit: AudioUnit?
            result = AUGraphNodeInfo(graph, node, &componentDescription, &audioUnit)
            if result != noErr {
                throw MIDIPlayerError.couldntGetNodeInfo(status: result)
            }

            if componentDescription.componentType == kAudioUnitType_MusicDevice {
                sampler = audioUnit
            } else if componentDescription.componentType == kAudioUnitType_Effect {
                compressorNode = node
            } else if componentDescription.componentType == kAudioUnitType_Mixer {
                mixer = audioUnit
            } else if componentDescription.componentType == kAudioUnitType_Output {
                outputNode = node
                output = audioUnit
            }
        }

        if sampler == nil {
            throw MIDIPlayerError.couldntFindSampler
        } else if compressorNode == nil {
            throw MIDIPlayerError.couldntFindCompressor
        } else if mixer == nil {
            throw MIDIPlayerError.couldntFindMixer
        } else if outputNode == nil {
            throw MIDIPlayerError.couldntFindOutputNode
        }

        // Insert the mixer node between the compressor and output
        result = AUGraphDisconnectNodeInput(graph, outputNode, 0)
        if result != noErr {
            throw MIDIPlayerError.couldntDisconnectOutputNode(status: result)
        }
        result = AUGraphConnectNodeInput(graph, compressorNode, 0, mixerNode, 0)
        if result != noErr {
            throw MIDIPlayerError.couldntConnectMixer(status: result)
        }
        result = AUGraphConnectNodeInput(graph, mixerNode, 0, outputNode, 0)
        if result != noErr {
            throw MIDIPlayerError.couldntConnectOutputNode(status: result)
        }

        // Now we can initialize the graph
        result = AUGraphInitialize(graph)
        if result != noErr {
            throw MIDIPlayerError.couldntInitializeGraph(status: result)
        }

        // Set up the music player
        result = NewMusicPlayer(&player)
        if result != noErr {
            throw MIDIPlayerError.couldntCreateMusicPlayer(status: result)
        }
        result = MusicPlayerSetSequence(player, sequence)
        if result != noErr {
            throw MIDIPlayerError.couldntSetSequence(status: result)
        }

        // Let the player do whatever it needs to do in advance of playing
        result = MusicPlayerPreroll(player)
        if result != noErr {
            throw MIDIPlayerError.couldntPrerollMusicPlayer(status: result)
        }
    }

    deinit {
        if player != nil {
            _ = MusicPlayerStop(player)
        }
        if graph != nil {
            _ = AUGraphUninitialize(graph)
            _ = AUGraphClose(graph)
        }
        // The graph is owned by the sequence so we don't need to dispose it
        if player != nil {
            _ = DisposeMusicPlayer(player)
        }
        if sequence != nil {
            _ = DisposeMusicSequence(sequence)
        }
    }


    func start() throws {
        if player == nil {
            return
        }

        let result = MusicPlayerStart(player)
        if result != noErr {
            throw MIDIPlayerError.couldntStartMusicPlayer(status: result)
        }
        playing = true
    }

    func stop() throws {
        if player == nil {
            return
        }
        
        let result = MusicPlayerStop(player)
        if result != noErr {
            throw MIDIPlayerError.couldntStopMusicPlayer(status: result)
        }
        playing = false
    }

    
    func setVolume(_ volume: Float32) throws {
        let volume = min(1, max(0, volume))

        let result = AudioUnitSetParameter(
            mixer, // inUnit
            kStereoMixerParam_Volume, // inID
            kAudioUnitScope_Output, // inScope
            0, // inElement
            volume, // inValue
            0 // inBufferOffsetInFrames
        )
        if result != noErr {
            throw MIDIPlayerError.couldntSetVolume(status: result)
        }
    }

    func setSoundFont(url soundFontURL: URL) throws {
        // Stop the music player first before switching the soundfont, this is required or the instruments will get messed up
        let wasPlaying = playing
        try stop()

        var soundFontURL = soundFontURL // the URL is passed inout to the next function so we can't use the function argument directly, we need to make a mutable copy
        var result = AudioUnitSetProperty(
            sampler, // inUnit
            kMusicDeviceProperty_SoundBankURL, // inID
            kAudioUnitScope_Global, // inScope
            0, // inElement
            &soundFontURL, // inData
            UInt32(MemoryLayout<URL>.stride) // inDataSize
        )
        if result != noErr {
            throw MIDIPlayerError.couldntLoadSoundFont(status: result)
        }

        if player == nil {
            return
        }

        result = MusicPlayerPreroll(player)
        if result != noErr {
            throw MIDIPlayerError.couldntPrerollMusicPlayer(status: result)
        }
        if wasPlaying {
            try start()
        }
    }


    func getTime() throws -> Double {
        var currentBeats: MusicTimeStamp = 0
        var result = MusicPlayerGetTime(player, &currentBeats)
        if result != noErr {
            throw MIDIPlayerError.couldntGetTime(status: result)
        }

        var currentTime: Float64 = 0
        result = MusicSequenceGetSecondsForBeats(sequence, currentBeats, &currentTime)
        if result != noErr {
            throw MIDIPlayerError.couldntConvertBeatsToSeconds(status: result)
        }
        return Double(currentTime)
    }

    func setTime(_ seconds: Double) throws {
        let seconds = max(0, seconds)

        var beats: MusicTimeStamp = 0
        var result = MusicSequenceGetBeatsForSeconds(sequence, Float64(seconds), &beats)
        if result != noErr {
            throw MIDIPlayerError.couldntConvertSecondsToBeats(status: result)
        }

        result = MusicPlayerSetTime(player, beats)
        if result != noErr {
            throw MIDIPlayerError.couldntSetTime(status: result)
        }
    }


}
