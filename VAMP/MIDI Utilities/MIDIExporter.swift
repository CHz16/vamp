//
//  MIDIExporter.swift
//  VAMP
//
//  Created by 'Ili Butterfield on 2/2/20.
//  Copyright © 2020 'Ili Butterfield. All rights reserved.
//

import AudioToolbox
import CoreAudioTypes
import Foundation


let framesPerBatch: UInt32 = 512 // this is the default from PlaySequence


enum MIDIExporterError: Error {
    case couldntSetSamplerToRenderOffline(status: OSStatus)
    case couldntGetDefaultOutputInfo(status: OSStatus)
    case couldntRemoveDefaultOutput(status: OSStatus)
    case couldntCreateOutput(status: OSStatus)
    case couldntGetOutputInfo(status: OSStatus)
    case couldntConnectOutputNode(status: OSStatus)
    case couldntCreateOutputFile(status: OSStatus)
    case couldntGetClientFormat(status: OSStatus)
    case couldntSetClientFormat(status: OSStatus)
    case couldntRenderAudio(status: OSStatus)
    case couldntWriteAudio(status: OSStatus)
}

class MIDIExporter {

    var player: MIDIPlayer

    init(file: MIDIFileInfo, soundFont: SoundFont) throws {
        player = try MIDIPlayer(url: file.url)
        try player.setSoundFont(url: soundFont.url)
        try player.setUpForExport()
    }

    func export(to outputURL: URL, duration: Double, startAt start: Double = 0) throws {
        // Create the output file
        var streamDescription = AudioStreamBasicDescription(mSampleRate: 44100, mFormatID: kAudioFormatLinearPCM, mFormatFlags: kLinearPCMFormatFlagIsSignedInteger | kLinearPCMFormatFlagIsPacked, mBytesPerPacket: 4, mFramesPerPacket: 1, mBytesPerFrame: 4, mChannelsPerFrame: 2, mBitsPerChannel: 16, mReserved: 0)
        var outputFile: ExtAudioFileRef!
        var result = ExtAudioFileCreateWithURL(
            outputURL as CFURL, // inURL
            kAudioFileWAVEType, // inFileType
            &streamDescription, // inStreamDesc
            nil, // inChannelLayout
            AudioFileFlags.eraseFile.rawValue, // inFlags
            &outputFile // outExtAudioFile
        )
        defer {
            // This also returns a result code that we might want to check, though since this is a defer block we can't throw an error if this randomly fails for some reason
            _ = ExtAudioFileDispose(outputFile)
        }
        if result != noErr {
            throw MIDIExporterError.couldntCreateOutputFile(status: result)
        }

        // Get the stream format from the output node
        var clientFormat = AudioStreamBasicDescription()
        var size = UInt32(MemoryLayout<AudioStreamBasicDescription>.stride)
        result = AudioUnitGetProperty(
            player.output, // inUnit
            kAudioUnitProperty_StreamFormat, // inID
            kAudioUnitScope_Output, // inScope
            0, // inElement
            &clientFormat, // outData
            &size // ioDataSize
        )
        if result != noErr {
            throw MIDIExporterError.couldntGetClientFormat(status: result)
        }

        // Give the stream format to the output file
        result = ExtAudioFileSetProperty(
            outputFile, // inExtAudioFile
            kExtAudioFileProperty_ClientDataFormat, // inPropertyID
            size, // inPropertyDataSize
            &clientFormat // inPropertyData
        )
        if result != noErr {
            throw MIDIExporterError.couldntSetClientFormat(status: result)
        }

        // Duration math
        // We process the audio in batches, and it's very likely that the export duration doesn't fit in an exact number of batches, so we calculate the number of frames so we know how much spillover there is at the end
        let startSample = ceil(start * 44100)
        let numberOfFrames = ceil(duration * 44100)
        let numberOfBatches = Int(ceil(numberOfFrames / Double(framesPerBatch)))
        let sizeOfLastBatch = UInt32(Int(numberOfFrames) % Int(framesPerBatch)) // not casting numberOfFrames to UInt32 immediately in case the user wants to export a 27 hour, 3 minute, and 12 second MIDI

        // Allocate the buffer lists
        var normalBufferSize = 4 * framesPerBatch
        var normalBufferList = AudioBufferList.allocate(maximumBuffers: 2)
        normalBufferList[0] = AudioBuffer(mNumberChannels: 1, mDataByteSize: normalBufferSize, mData: malloc(Int(normalBufferSize)))
        normalBufferList[1] = AudioBuffer(mNumberChannels: 1, mDataByteSize: normalBufferSize, mData: malloc(Int(normalBufferSize)))
        var lastBufferSize = 4 * sizeOfLastBatch
        var lastBufferList = AudioBufferList.allocate(maximumBuffers: 2)
        lastBufferList[0] = AudioBuffer(mNumberChannels: 1, mDataByteSize: lastBufferSize, mData: malloc(Int(lastBufferSize)))
        lastBufferList[1] = AudioBuffer(mNumberChannels: 1, mDataByteSize: lastBufferSize, mData: malloc(Int(lastBufferSize)))
        defer {
            free(normalBufferList[0].mData)
            free(normalBufferList[1].mData)
            free(normalBufferList.unsafeMutablePointer)
            free(lastBufferList[0].mData)
            free(lastBufferList[1].mData)
            free(lastBufferList.unsafeMutablePointer)
        }

        // Start up the player so we can grab its audio
        // We probably won't ever reuse one of these exporter classes, but in case we do, reset the time to 0 first
        try player.setTime(start)
        try player.start()
        defer {
            try? player.stop()
        }

        // Rendering time!
        var renderTimestamp = AudioTimeStamp()
        renderTimestamp.mSampleTime = startSample
        for i in 0..<numberOfBatches {
            let bufferList = (i == numberOfBatches - 1) ? lastBufferList : normalBufferList
            let numberOfFrames = (i == numberOfBatches - 1) ? sizeOfLastBatch : framesPerBatch

            // Render to a buffer
            var flags: AudioUnitRenderActionFlags = []
            result = AudioUnitRender(
                player.output, // inUnit
                &flags, // ioActionFlags
                &renderTimestamp, // inTimeStamp
                0, // inOutputBusNumber
                numberOfFrames, // inNumberFrames
                bufferList.unsafeMutablePointer // ioData
            )
            if result != noErr {
                throw MIDIExporterError.couldntRenderAudio(status: result)
            }

            // Write the buffer to disk
            result = ExtAudioFileWrite(
                outputFile, // inExtAudioFile
                numberOfFrames, // inNumberFrames
                bufferList.unsafePointer // ioData
            )
            if result != noErr {
                throw MIDIExporterError.couldntWriteAudio(status: result)
            }

            // Tick
            renderTimestamp.mSampleTime += Float64(numberOfFrames)
        }

        // Resource cleanup is in various defer blocks
    }

}

extension MIDIPlayer {

    func setUpForExport() throws {
        // Set the sampler to render offline
        var offlineRender: UInt32 = 1
        var result = AudioUnitSetProperty(
            sampler, // inUnit
            kAudioUnitProperty_OfflineRender, // inID
            kAudioUnitScope_Global, // inScope
            0, // inElement
            &offlineRender, // inData
            UInt32(MemoryLayout<UInt32>.stride) // inDataSize
        )
        if result != noErr {
            throw MIDIExporterError.couldntSetSamplerToRenderOffline(status: result)
        }

        // Get the description of the output node that MusicSequenceGetAUGraph made; we'll use this to make a new one later
        var outputDescription = AudioComponentDescription()
        result = AUGraphNodeInfo(graph, outputNode, &outputDescription, nil)
        if result != noErr {
            throw MIDIExporterError.couldntGetDefaultOutputInfo(status: result)
        }

        // Remove the old output node
        result = AUGraphRemoveNode(graph, outputNode)
        if result != noErr {
            throw MIDIExporterError.couldntRemoveDefaultOutput(status: result)
        }

        // Create a new GenericOutput
        outputDescription.componentSubType = kAudioUnitSubType_GenericOutput
        result = AUGraphAddNode(graph, &outputDescription, &outputNode)
        if result != noErr {
             throw MIDIExporterError.couldntCreateOutput(status: result)
        }

        result = AUGraphNodeInfo(graph, outputNode, nil, &output)
        if result != noErr {
            throw MIDIExporterError.couldntGetOutputInfo(status: result)
        }

        // Connect the mixer to the new output
        result = AUGraphConnectNodeInput(graph, mixerNode, 0, outputNode, 0)
        if result != noErr {
            throw MIDIExporterError.couldntConnectOutputNode(status: result)
        }

        
    }

}
