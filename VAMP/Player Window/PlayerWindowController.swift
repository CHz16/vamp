//
//  PlayerWindowController.swift
//  VAMP
//
//  Created by 'Ili Butterfield on 1/20/20.
//  Copyright © 2020 'Ili Butterfield. All rights reserved.
//

import AppKit
import Foundation


class PlayerWindowController: NSWindowController {
    
}

extension PlayerWindowController: NSWindowDelegate {

    func windowDidChangeScreen(_ notification: Notification) {
        (window!.contentViewController! as! PlayerViewController).createDisplayLink()
    }

}
