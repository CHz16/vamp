//
//  PlayerViewController+TableView.swift
//  VAMP
//
//  Created by 'Ili Butterfield on 1/17/20.
//  Copyright © 2020 'Ili Butterfield. All rights reserved.
//

import AppKit
import Foundation


enum TracklistCellIdentifier {
    static let playingCell = NSUserInterfaceItemIdentifier(rawValue: "playing")
    static let titleCell = NSUserInterfaceItemIdentifier(rawValue: "title")
    static let durationCell = NSUserInterfaceItemIdentifier(rawValue: "duration")
}

extension NSPasteboard.PasteboardType {
    static let trackPasteboardType = NSPasteboard.PasteboardType(rawValue: "com.quiteajolt.VAMP.track-row")
}


// MARK: NSTableViewDataSource

extension PlayerViewController: NSTableViewDataSource {

    func numberOfRows(in tableView: NSTableView) -> Int {
        return Playlist.tracks.count
    }

    func tableView(_ tableView: NSTableView, sortDescriptorsDidChange oldDescriptors: [NSSortDescriptor]) {
        guard let newSortDescriptor = tableView.sortDescriptors.first else {
            return
        }
        guard let newSortDescriptorKey = newSortDescriptor.key else {
            return
        }

        // We want to keep the user's current selection after sorting, but by the time we're in this function, the selection has already been cleared. So, we'll manually save whatever the last selection was so we can use it here.
        // This is a heavy effort because we have to track the changing selection in multiple places lmao. Trapping tableViewSelectionDidChange(_:) isn't an option because it also fires when the sort clears the selection, and it also doesn't fire on all changes we want anyway. Here's the master list of update places:
        // • tableView(_:selectionIndexesForProposedSelection:proposedSelectionIndexes:) - User selecting and deselecting rows
        // • tableView(_:acceptDrop:row:dropOperation:) - User dragging something into the playlist or manually reordering it
        // • deleteBackward(_:) - User deleting row
        // • And of course, this function at the bottom when we update the selection after sorting
        let selectedTrackUUIDs = lastSelectedRowIndexes.map { Playlist.tracks[$0].uuid }

        // Sort the model and then update the table
        if newSortDescriptorKey == TracklistCellIdentifier.titleCell.rawValue {
            Playlist.tracks = Playlist.tracks.sorted {
                let ascending = $0.filename.localizedStandardCompare($1.filename) == .orderedAscending
                return (newSortDescriptor.ascending) ? ascending : !ascending
            }
        } else if newSortDescriptorKey == TracklistCellIdentifier.durationCell.rawValue {
            Playlist.tracks = Playlist.tracks.sorted {
                let ascending = $0.duration < $1.duration
                return (newSortDescriptor.ascending) ? ascending : !ascending
            }
        }
        tableView.reloadData()

        // Look up the new indexes of the tracks with the saved UUIDs and make that the new selection
        let newRows = Playlist.tracks.enumerated().filter { selectedTrackUUIDs.contains($0.element.uuid) }.map { $0.offset }
        let newSelectionIndexes = IndexSet(newRows)
        lastSelectedRowIndexes = newSelectionIndexes
        tableView.selectRowIndexes(newSelectionIndexes, byExtendingSelection: false)
    }

    func tableView(_ tableView: NSTableView, pasteboardWriterForRow row: Int) -> NSPasteboardWriting? {
        let pasteboardItem = NSPasteboardItem()
        pasteboardItem.setPropertyList(row, forType: .trackPasteboardType) // lmao using an int as a property list don't worry about it
        return pasteboardItem
    }

}


// MARK: NSTableViewDelegate

extension PlayerViewController: NSTableViewDelegate {

    func tableView(_ tableView: NSTableView, viewFor tableColumn: NSTableColumn?, row: Int) -> NSView? {
        guard let identifier = tableColumn?.identifier else {
            return nil
        }
        guard let cell = tableView.makeView(withIdentifier: identifier, owner: self) as? NSTableCellView else {
            return nil
        }

        if identifier == TracklistCellIdentifier.playingCell {
            if let uuid = activeTrackUUID, uuid == Playlist.tracks[row].uuid {
                cell.imageView?.image = isPlaying ? NSImage(named: NSImage.touchBarPlayTemplateName) : NSImage(named: NSImage.touchBarPauseTemplateName)
            } else {
                cell.imageView?.image = nil
            }
        } else if identifier == TracklistCellIdentifier.titleCell {
            cell.textField?.stringValue = Playlist.tracks[row].longTitle
        } else if identifier == TracklistCellIdentifier.durationCell {
            cell.textField?.stringValue = formatTimestampAsHMS(Playlist.tracks[row].duration)
        }
        return cell
    }

    func tableView(_ tableView: NSTableView, selectionIndexesForProposedSelection proposedSelectionIndexes: IndexSet) -> IndexSet {
        // Save the new selection for use in tableView(_:sortDescriptorsDidChange:oldDescriptors:) 
        lastSelectedRowIndexes = proposedSelectionIndexes
        return proposedSelectionIndexes
    }

    func tableViewSelectionDidChange(_ notification: Notification) {
        // Enable/disable the delete song button based on whether anything's selected or not
        // This hook should fire even if we change the selection programmatically (such as when we delete something), which is ideal
        deleteSongButton.isEnabled = (tableView.selectedRowIndexes.count > 0)
    }

}


// MARK: NSDraggingDestination

extension PlayerViewController: NSDraggingDestination {

    func makeURLs(from info: NSDraggingInfo) -> [URL]? {
        return info.draggingPasteboard.readObjects(forClasses: [NSURL.self], options: [.urlReadingContentsConformToTypes: ["public.midi-audio", "public.folder"]]) as? [URL]
    }

    func tableView(_ tableView: NSTableView, shouldReorderColumn columnIndex: Int, toColumn newColumnIndex: Int) -> Bool {
        // Don't allow the first column (the playing/paused icon) to be moved
        return (columnIndex != 0 && newColumnIndex != 0)
    }

    func tableView(_ tableView: NSTableView, draggingSession session: NSDraggingSession, willBeginAt screenPoint: NSPoint, forRowIndexes rowIndexes: IndexSet) {
        // This code assumes that the order the dragging items are processed is the same as the order of the indexes in Array(rowIndexes), which I'm not necessarily sure is a safe assumption, but it seems to work after quick testing
        let titleColumn = tableView.column(withIdentifier: TracklistCellIdentifier.titleCell)
        let indexArray = Array(rowIndexes) // convert this to an array so we can index into it in the following closure
        session.enumerateDraggingItems(options: .concurrent, for: tableView, classes: [NSPasteboardItem.self], searchOptions: [:]) { (draggingItem, idx, stop) in
            if let view = tableView.view(atColumn: titleColumn, row: indexArray[idx], makeIfNecessary: false) as? NSTableCellView {
                draggingItem.imageComponentsProvider = { view.draggingImageComponents }
            }
        }
    }

    func tableView(_ tableView: NSTableView, validateDrop info: NSDraggingInfo, proposedRow row: Int, proposedDropOperation dropOperation: NSTableView.DropOperation) -> NSDragOperation {
        guard let firstItem = info.draggingPasteboard.pasteboardItems?[0] else {
            return []
        }

        // Check if the user is dragging a row in the table
        if firstItem.data(forType: .trackPasteboardType) != nil {
            if dropOperation != .above {
                return []
            }
            return .move
        }

        // Otherwise, they're dragging files
        guard let urls = makeURLs(from: info) else {
            return []
        }
        if urls.isEmpty || dropOperation != .above {
            return []
        }
        return .move
    }

    func tableView(_ tableView: NSTableView, acceptDrop info: NSDraggingInfo, row: Int, dropOperation: NSTableView.DropOperation) -> Bool {
        guard let firstItem = info.draggingPasteboard.pasteboardItems?[0] else {
            return false
        }

        // Check if the user is dragging a row in the table
        if firstItem.data(forType: .trackPasteboardType) != nil {
            guard let draggedRows = info.draggingPasteboard.pasteboardItems?.compactMap({ $0.propertyList(forType: .trackPasteboardType) as? Int }) else {
                return false
            }
            if draggedRows.isEmpty {
                return false
            }

            // For reordering multiple selections, what we'll do is put the entire selection, in order, at the spot the user drags it to.
            // So for the model, what we'll do is collect every one of those items into an array, delete each of those items from the original array, and then insert the new array into the insertion spot.
            // The tricky part is that, if we delete items from before the insertion spot, then the insertion spot will change indexes. So to handle that, we'll count how many of those dragged items were before the insertion spot and then subtract that from the insertion spot.
            let draggedItems = draggedRows.map { Playlist.tracks[$0] }
            Playlist.tracks.remove(at: IndexSet(draggedRows))
            let insertRow = row - draggedRows.filter { $0 < row }.count
            Playlist.tracks.insert(contentsOf: draggedItems, at: insertRow)

            tableView.reloadData()

            // Set the selection to the new locations of the moved rows and save the new selection for use in tableView(_:sortDescriptorsDidChange:oldDescriptors:)
            // reloadData() clears the current selection, so we have to do this
            let newSelectionIndexes = IndexSet(insertRow..<(insertRow + draggedItems.count))
            tableView.selectRowIndexes(newSelectionIndexes, byExtendingSelection: false)
            lastSelectedRowIndexes = newSelectionIndexes

            return true
        }

        // Otherwise, they're dragging files
        guard let urls = makeURLs(from: info) else {
            return false
        }
        if urls.isEmpty {
            return false
        }
        addURLsToPlaylist(urls, row: row)

        // Save the new selection for use in tableView(_:sortDescriptorsDidChange:oldDescriptors:)
        // If the user dragged a file above the selection, then its position shifted.
        lastSelectedRowIndexes = tableView.selectedRowIndexes

        return true
    }

}
