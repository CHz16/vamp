//
//  PlayerViewController.swift
//  VAMP
//
//  Created by 'Ili Butterfield on 1/14/20.
//  Copyright © 2020 'Ili Butterfield. All rights reserved.
//

import AppKit
import AudioToolbox
import CoreVideo
import Foundation


extension NSNotification.Name {
    static let currentTrackInfosUpdated = NSNotification.Name(rawValue: "com.quiteajolt.VAMP.currentTrackInfosUpdated")
}


class PlayerViewController: NSViewController {

    @IBOutlet weak var playButton: NSButton!
    @IBOutlet weak var stopButton: NSButton!
    @IBOutlet weak var songPositionSlider: TrackingSlider!
    @IBOutlet weak var songPositionLabel: NSTextField!
    @IBOutlet weak var volumeSlider: NSSlider!
    @IBOutlet weak var tableView: NSTableView!
    @IBOutlet weak var deleteSongButton: NSButton!
    @IBOutlet weak var soundFontsPopUp: NSPopUpButton!

    var songPaddingObserver: NSKeyValueObservation!

    var showTimeRemaining = false

    var midiPlayer: MIDIPlayer!
    var activeTrackUUID: UUID?
    var activeTrackPaddedDuration: Double?
    var isPlaying = false

    var displayLink: CVDisplayLink!

    var lastSelectedRowIndexes = IndexSet()

    override func viewDidLoad() {
        super.viewDidLoad()

        songPaddingObserver = Preferences.standard.observe(\.songPadding, options: [.old, .new], changeHandler: songPaddingPreferenceChanged)
        NotificationCenter.default.addObserver(self, selector: #selector(updateSoundFontsPopUp), name: .soundFontsModified, object: nil)

        volumeSlider.doubleValue = Preferences.standard.volume

        let clickGestureRecognizer = NSClickGestureRecognizer(target: self, action: #selector(songPositionLabelClicked(gestureRecognizer:)))
        songPositionLabel.addGestureRecognizer(clickGestureRecognizer)

        tableView.dataSource = self
        tableView.delegate = self

        tableView.registerForDraggedTypes([.URL, .trackPasteboardType])

        tableView.tableColumn(withIdentifier: TracklistCellIdentifier.titleCell)?.sortDescriptorPrototype = NSSortDescriptor(key: TracklistCellIdentifier.titleCell.rawValue, ascending: true)
        tableView.tableColumn(withIdentifier: TracklistCellIdentifier.durationCell)?.sortDescriptorPrototype = NSSortDescriptor(key: TracklistCellIdentifier.durationCell.rawValue, ascending: true)

        tableView.target = self
        tableView.doubleAction = #selector(handleDoubleClick(_:))

        updateControls()
        updateSoundFontsPopUp()
    }

    override func viewDidAppear() {
        // The window needs to exist when we create the display link, so this needs to happen here instead of viewDidLoad
        createDisplayLink()
    }

    override var representedObject: Any? {
        didSet {
        // Update the view, if already loaded.
        }
    }

    deinit {
        if let songPaddingObserver = songPaddingObserver {
            Preferences.standard.removeObserver(songPaddingObserver, forKeyPath: "songPadding")
        }
        NotificationCenter.default.removeObserver(self)
    }


}


// MARK: UI - window controls

extension PlayerViewController {

    func updateControls() {
        playButton.image = isPlaying ? NSImage(named: NSImage.touchBarPauseTemplateName) : NSImage(named: NSImage.touchBarPlayTemplateName)
        playButton.isEnabled = !Playlist.tracks.isEmpty
        (NSApp.delegate! as! AppDelegate).setPlayPauseMenuItemTitle(isPlaying ? NSLocalizedString("pauseMenuItem", comment: "pause menu item title") : NSLocalizedString("playMenuItem", comment: "play menu item title"))
        stopButton.isEnabled = (midiPlayer != nil)
        updateSongPosition()
    }

    func updateSongPosition(timestamp inTimestamp: Double? = nil) {
        if midiPlayer == nil {
            songPositionSlider.isEnabled = false
            songPositionSlider.doubleValue = 0
            songPositionLabel.stringValue = (showTimeRemaining ? "-0:00" : "0:00")
            return
        }

        var timestamp: Double
        if inTimestamp != nil {
            timestamp = inTimestamp!
        } else {
            do {
                timestamp = try midiPlayer.getTime()
            } catch {
                showErrorInCurrentWindow(key: "error.couldntGetSongPosition", error: error)
                stopTrack()
                return
            }
        }

        // If the current song is over, then stop it and automatically play the next one if that wasn't the last song in the playlist.
        // We *won't* stop the current song if the user is currently scrubbing through the position slider, so that they can move the slider to the end and back without having the song switch out from under them.
        if timestamp > activeTrackPaddedDuration! && !songPositionSlider.isTracking {
            let track = indexOfActiveSong()!
            stopTrack()
            if track < Playlist.tracks.count - 1 {
                loadTrack(track + 1)
            }
            // Both stopTrack() and loadTrack() will call back into this function and set the UI accordingly, so we don't need to do any more work here.
            return
        }

        timestamp = min(timestamp, activeTrackPaddedDuration!) // In case the playhead has gone past the end of the song, per above
        songPositionSlider.doubleValue = timestamp
        if showTimeRemaining {
            songPositionLabel.stringValue = formatTimestampAsHMS(timestamp - activeTrackPaddedDuration!)
        } else {
            songPositionLabel.stringValue = formatTimestampAsHMS(timestamp)
        }
    }

    @IBAction func playButtonClicked(_ sender: Any) {
        if midiPlayer == nil {
            playNewSong()
        } else {
            togglePause()
        }
    }

    @IBAction func stopButtonClicked(_ sender: Any) {
        stopTrack()
    }

    @IBAction func volumeSliderChanged(_ sender: Any) {
        try? setVolume()
    }

    @IBAction func songPositionSliderChanged(_ sender: Any) {
        // Dragging on trackpad will produce one final duplicate event at the end when the drag ends, which will cause an audible skip, so we'll ignore that final "position change."
        if !songPositionSlider.isTracking {
            return
        }

        do {
            try midiPlayer.setTime(songPositionSlider.doubleValue)
            updateSongPosition(timestamp: songPositionSlider.doubleValue)
        } catch {
            showErrorInCurrentWindow(key: "error.couldntSetPlayerPosition", error: error)
            stopTrack()
        }
    }

    @objc func songPositionLabelClicked(gestureRecognizer: NSGestureRecognizer) {
        showTimeRemaining = !showTimeRemaining
        updateSongPosition()
    }

    @objc func updateSoundFontsPopUp() {
        soundFontsPopUp.removeAllItems()
        soundFontsPopUp.addItems(withTitles: SoundFonts.array.map { $0.name })
        if let i = SoundFonts.indexOfSoundFontWithUUID(Preferences.standard.selectedSoundFontUUID) {
            soundFontsPopUp.selectItem(at: i)
        } else {
            let i = SoundFonts.indexOfSystemSoundFont
            soundFontsPopUp.selectItem(at: i)
            soundFontsPopUpChanged(self)
        }
    }

    @IBAction func soundFontsPopUpChanged(_ sender: Any) {
        if Preferences.standard.selectedSoundFontUUID == SoundFonts[soundFontsPopUp.indexOfSelectedItem].uuid {
            return
        }

        Preferences.standard.selectedSoundFontUUID = SoundFonts[soundFontsPopUp.indexOfSelectedItem].uuid
        try? setSoundFont()
    }

}


// MARK: UI - menus

extension PlayerViewController {

    @IBAction func showInFinderMenuItemSelected(_ sender: Any) {
        showMIDIFilesInFinder(indexes: tableView.selectedRowIndexes)
    }

    func increaseVolume() {
        volumeSlider.doubleValue += 5.0
        try? setVolume()
    }

    func decreaseVolume() {
        volumeSlider.doubleValue -= 5.0
        try? setVolume()
    }

    @IBAction func playContextualMenuItemSelected(_ sender: Any) {
        playNewSong()
    }

    @IBAction func exportContextualMenuItemSelected(_ sender: Any) {
        exportMIDI(index: tableView.selectedRow)
    }

    @IBAction func copy(_ sender: Any) {
        let titleFirst = tableView.tableColumns[1].identifier == TracklistCellIdentifier.titleCell

        var songRows: [String] = []
        for i in tableView.selectedRowIndexes {
            if let refreshedFileInfo = try? Playlist.tracks[i].refresh() {
                Playlist.tracks[i] = refreshedFileInfo
                tableView.reloadData(forRowIndexes: IndexSet(integer: i), columnIndexes: IndexSet(0..<tableView.tableColumns.count))
            }
            let fileInfo = Playlist.tracks[i]
            let formattedDuration = formatTimestampAsHMS(fileInfo.duration)
            if titleFirst {
                songRows.append("\(fileInfo.longTitle)\t\(formattedDuration)")
            } else {
                songRows.append("\(formattedDuration)\t\(fileInfo.longTitle)")
            }
        }

        let pasteboard = NSPasteboard.general
        pasteboard.clearContents()
        pasteboard.setString(songRows.joined(separator: "\n"), forType: .string)
    }

    @IBAction func delete(_ sender: Any) {
        deleteBackward(self)
    }

}


// MARK: NSUserInterfaceValidations

extension PlayerViewController: NSUserInterfaceValidations {

    func validateUserInterfaceItem(_ item: NSValidatedUserInterfaceItem) -> Bool {
        guard let action = item.action else {
            return false
        }

        // When the user right clicks in the table view, we want to select the row they clicked on as the target for the actions. This function... seems like the best place to do it?
        // We'll also deselect any rows if the user right clicks in a row in the table that doesn't exist. However, since this function is also called to check items in the main menu, tableView.clickedRow will be -1 during those validations too. So we'll check if we're showing the table contextual menu by looking at the tag of the item: all items there are tagged with 1
        if tableView.clickedRow != -1 {
            tableView.selectRowIndexes(IndexSet(integer: tableView.clickedRow), byExtendingSelection: false)
        } else if item.tag == 1 {
            tableView.selectRowIndexes(IndexSet(), byExtendingSelection: false)
        }
        
        switch action {
        case #selector(copy(_:)), #selector(showInFinderMenuItemSelected(_:)), #selector(exportMIDI(index:)), #selector(playContextualMenuItemSelected(_:)), #selector(delete(_:)):
            return !tableView.selectedRowIndexes.isEmpty
        case #selector(AppDelegate.playMenuItemSelected(_:)):
            return !Playlist.tracks.isEmpty
        case #selector(AppDelegate.exportMenuItemSelected(_:)), #selector(AppDelegate.stopMenuItemSelected(_:)), #selector(AppDelegate.nextMenuItemSelected(_:)), #selector(AppDelegate.previousMenuItemSelected(_:)):
            return (midiPlayer != nil)
        default:
            return true
        }
    }

}


// MARK: CVDisplayLink

extension PlayerViewController {

    func createDisplayLink() {
        var result: CVReturn

        // The window controller will call this function if the window changes screens, in which case we'll set up a new display link for the new display
        if displayLink != nil && CVDisplayLinkIsRunning(displayLink) {
            result = CVDisplayLinkStop(displayLink)
            if result != kCVReturnSuccess {
                showErrorInCurrentWindow(key: "error.couldntStopDisplayLink", terminateAfter: true)
                return
            }
        }

        // Create the display link
        let displayID = view.window!.screen!.deviceDescription[NSDeviceDescriptionKey("NSScreenNumber")]! as! CGDirectDisplayID
        result = CVDisplayLinkCreateWithCGDisplay(displayID, &displayLink)
        if result != kCVReturnSuccess {
            showErrorInCurrentWindow(key: "error.couldntCreateDisplayLink", terminateAfter: true)
            return
        }

        // Set the callback for the display link
        result = CVDisplayLinkSetOutputHandler(displayLink, displayLinkHandler)
        if result != kCVReturnSuccess {
            showErrorInCurrentWindow(key: "error.couldntSetDisplayLinkHandler", terminateAfter: true)
            return
        }

        // Start up the display link immediately if the player is playing; this will happen if the screen changes while the player is going
        if isPlaying {
            startDisplayLink()
        }
    }

    func startDisplayLink() {
        if CVDisplayLinkIsRunning(displayLink) {
            return
        }

        let result = CVDisplayLinkStart(displayLink)
        if result != kCVReturnSuccess {
            showErrorInCurrentWindow(key: "error.couldntStartDisplayLink", terminateAfter: true)
        }
    }

    func stopDisplayLink() {
        if !CVDisplayLinkIsRunning(displayLink) {
            return
        }

        let result = CVDisplayLinkStop(displayLink)
        if result != kCVReturnSuccess {
            showErrorInCurrentWindow(key: "error.couldntStopDisplayLink", terminateAfter: true)
        }
    }

    func displayLinkHandler(inDisplayLink: CVDisplayLink, inNow: UnsafePointer<CVTimeStamp>, inOutputTime: UnsafePointer<CVTimeStamp>, flagsIn: CVOptionFlags, flagsOut: UnsafeMutablePointer<CVOptionFlags>) -> CVReturn {
        // This callback runs off the main thread, so we need to send this back there to update the UI
        // Doing less work here will also reduce the number of opportunities for race conditions, which I'm extremely interested in
        DispatchQueue.main.async {
            self.updateSongPosition()
        }
        return kCVReturnSuccess
    }

}


// MARK: preferences syncing

extension PlayerViewController {

    func songPaddingPreferenceChanged(model: Preferences, change: NSKeyValueObservedChange<Int>) {
        if midiPlayer != nil {
            let durationChange = Double(change.newValue! - change.oldValue!)
            activeTrackPaddedDuration! += durationChange
            songPositionSlider.maxValue += durationChange
        }
    }

}


// MARK: MIDI player control

extension PlayerViewController {

    // The index can change after sorting or adding new songs to the playlist, so we'll get it dynamically when we need it
    func indexOfActiveSong() -> Int? {
        guard let uuid = activeTrackUUID else { return nil }
        return Playlist.tracks.firstIndex(where: { $0.uuid == uuid })
    }

    func playNewSong() {
        if let firstIndex = tableView.selectedRowIndexes.first {
            loadTrack(firstIndex)
        } else if !Playlist.tracks.isEmpty {
            loadTrack(0)
        }
    }

    func stopTrack() {
        if midiPlayer == nil {
            return
        }

        // This used to be later in the function, after the MIDI player was stopped, but there was a race condition because the display link callback referenced the MIDI player and could execute between when the player was destroyed and when the callback was stopped, so I moved it here.
        // This race condition doesn't exist any more after some code reorganization, but I'm leaving this here instead of moving it back. It's probably better here anyway?
        stopDisplayLink()

        do {
            try midiPlayer.stop()
        } catch {
            // This is probably bad
            showErrorInCurrentWindow(key: "error.couldntStopPlayer", error: error)
        }

        // Trying to dealloc the MIDI player when the last bit threw an exception could be problematic, but things have gone horribly awry if that last block threw anyway.
        midiPlayer = nil
        isPlaying = false

        let track = indexOfActiveSong()!
        activeTrackUUID = nil
        activeTrackPaddedDuration = nil
        tableView.reloadData(forRowIndexes: IndexSet(integer: track), columnIndexes: IndexSet(integer: 0))
        updateControls()

        CurrentTrackInfos.infos = []
        NotificationCenter.default.post(name: .currentTrackInfosUpdated, object: self)
    }

    func loadTrack(_ track: Int, play: Bool = true) {
        // Stop and unload any currently playing track
        if midiPlayer != nil {
            stopTrack()
        }

        // Refresh the info for the selected MIDI file, in case it was altered or something
        let refreshedFileInfo: MIDIFileInfo
        do {
            refreshedFileInfo = try Playlist.tracks[track].refresh()
        } catch {
            showErrorInCurrentWindow(key: "error.couldntOpenFile", informationArguments: ["\(Playlist.tracks[track].url.path)"], error: error)
            return
        }
        Playlist.tracks[track] = refreshedFileInfo

        // Try to create the player
        do {
            midiPlayer = try MIDIPlayer(url: refreshedFileInfo.url)
        } catch {
            showErrorInCurrentWindow(key: "error.couldntLoadFile", informationArguments: ["\(refreshedFileInfo.url.path)"], error: error)
            return
        }

        // Try to set the soundfont
        do {
            try setSoundFont()
        } catch {
            return
        }

        // Try to set the volume
        do {
            try setVolume()
        } catch {
            return
        }

        // Try to start the player
        if play {
            do {
                try midiPlayer.start()
            } catch MIDIPlayerError.couldntStartMusicPlayer(let status) where status == kAudioToolboxErr_InvalidPlayerState {
                midiPlayer = nil
                showErrorInCurrentWindow(key: "error.possiblyCorruptFile", informationArguments: ["\(refreshedFileInfo.url.path)"], error: MIDIPlayerError.couldntStartMusicPlayer(status: kAudioToolboxErr_InvalidPlayerState))
                return
            } catch {
                midiPlayer = nil
                showErrorInCurrentWindow(key: "error.couldntPlayFile", informationArguments: ["\(refreshedFileInfo.url.path)"], error: error)
                return
            }
        }

        // We made it! Let's update the UI and model now
        isPlaying = play
        if isPlaying {
            startDisplayLink()
        }

        activeTrackUUID = Playlist.tracks[track].uuid
        activeTrackPaddedDuration = Playlist.tracks[track].duration + Double(Preferences.standard.songPadding)
        songPositionSlider.isEnabled = true
        songPositionSlider.maxValue = activeTrackPaddedDuration!
        tableView.reloadData(forRowIndexes: IndexSet(integer: track), columnIndexes: IndexSet(0..<tableView.tableColumns.count))
        updateControls()

        CurrentTrackInfos.infos = Playlist.tracks[track].trackInfos
        NotificationCenter.default.post(name: .currentTrackInfosUpdated, object: self)
    }

    func togglePause() {
        do {
            if isPlaying {
                try midiPlayer.stop()
                stopDisplayLink()
            } else {
                try midiPlayer.start()
                startDisplayLink()
            }
        } catch {
            if isPlaying {
                showErrorInCurrentWindow(key: "error.couldntPausePlayer", error: error)
            } else {
                showErrorInCurrentWindow(key: "error.couldntResumePlayer", error: error)
            }
            stopTrack()
            return
        }

        isPlaying = !isPlaying
        let track = indexOfActiveSong()!
        tableView.reloadData(forRowIndexes: IndexSet(integer: track), columnIndexes: IndexSet(integer: 0))
        updateControls()
    }

    func goToPrevious() {
        goToTrack(indexOfActiveSong()! - 1)
    }

    func goToNext() {
        goToTrack(indexOfActiveSong()! + 1)
    }

    func goToTrack(_ track: Int) {
        if midiPlayer == nil {
            return
        }

        let currentlyPlaying = isPlaying
        if track >= 0 && track < Playlist.tracks.count {
            loadTrack(track, play: currentlyPlaying)
        } else {
            stopTrack()
        }
    }

    func setVolume() throws {
        Preferences.standard.volume = volumeSlider.doubleValue
        if midiPlayer == nil {
            return
        }

        do {
            try midiPlayer.setVolume(Float32(volumeSlider.doubleValue / volumeSlider.maxValue))
        } catch {
            showErrorInCurrentWindow(key: "error.couldntChangeVolume", error: error)
            stopTrack()
            throw error
        }
    }

    func setSoundFont() throws {
        guard let midiPlayer = midiPlayer else {
            return
        }

        let soundFont: SoundFont
        do {
            soundFont = try SoundFonts.refresh(index: soundFontsPopUp.indexOfSelectedItem)
        } catch {
            showErrorInCurrentWindow(key: "error.couldntLoadSoundFont", informationArguments: [SoundFonts[soundFontsPopUp.indexOfSelectedItem].name], error: error)
            stopTrack()
            self.midiPlayer = nil
            throw error
        }

        do {
            try midiPlayer.setSoundFont(url: soundFont.url)
        } catch {
            showErrorInCurrentWindow(key: "error.couldntSetSoundFont", informationArguments: [soundFont.name], error: error)
            stopTrack()
            self.midiPlayer = nil
            throw error
        }
    }

}


// MARK: file handling

extension PlayerViewController {

    @IBAction func openDocument(_ sender: Any) {
        let openPanel = NSOpenPanel()
        openPanel.allowsMultipleSelection = true
        openPanel.canChooseDirectories = true
        openPanel.allowedFileTypes = ["public.midi-audio"]
        openPanel.begin { result in
            if result != .OK {
                return
            }
            self.addURLsToPlaylist(openPanel.urls, row: Playlist.tracks.count)
        }
    }

    func addURLsToPlaylist(_ urls: [URL], row: Int) {
        var newTracks: [MIDIFileInfo] = []
        for url in urls {
            // Check if the url is a MIDI file or directory
            guard let resourceValues = try? url.resourceValues(forKeys: [.isDirectoryKey]) else {
                continue
            }
            guard let isDirectory = resourceValues.isDirectory else {
                continue
            }

            if isDirectory {
                // If it's a directory, then just do a shallow enumeration of the directory's contents and add every MIDI that we can successfully load
                guard let directoryContents = try? FileManager.default.contentsOfDirectory(at: url, includingPropertiesForKeys: [.typeIdentifierKey], options: []) else {
                    continue
                }
                for subURL in directoryContents {
                    guard let subResourceValues = try? subURL.resourceValues(forKeys: [.typeIdentifierKey]) else {
                        continue
                    }
                    guard let typeIdentifier = subResourceValues.typeIdentifier else {
                        continue
                    }

                    if typeIdentifier == "public.midi-audio" {
                        do {
                            newTracks.append(try MIDIFileInfo(url: subURL))
                        } catch {
                            continue
                        }
                    }
                }
            } else {
                // If it's a MIDI file then just add it if we can load it
                do {
                    newTracks.append(try MIDIFileInfo(url: url))
                } catch {
                    continue
                }
            }
        }

        if !newTracks.isEmpty {
            Playlist.tracks.insert(contentsOf: newTracks, at: row)
            tableView.insertRows(at: IndexSet(row..<(row + newTracks.count)), withAnimation: [.slideDown])
            updateControls()

            for track in newTracks {
                NSDocumentController.shared.noteNewRecentDocumentURL(track.url)
            }
        }
    }

    func showMIDIFilesInFinder(indexes: IndexSet) {
        var urls: [URL] = []
        for index in indexes {
            do {
                Playlist.tracks[index] = try Playlist.tracks[index].refresh()
                urls.append(Playlist.tracks[index].url)
            } catch {
                showErrorInCurrentWindow(key: "error.couldntOpenFile", informationArguments: ["\(Playlist.tracks[index].url.path)"], error: error)
            }
        }
        if !urls.isEmpty {
            NSWorkspace.shared.activateFileViewerSelecting(urls)
        }
    }

    @objc func exportMIDI(index inIndex: Int = -1) {
        let index = (inIndex != -1) ? inIndex : indexOfActiveSong()!
        let refreshedFileInfo: MIDIFileInfo
        do {
            refreshedFileInfo = try Playlist.tracks[index].refresh()
        } catch {
            showErrorInCurrentWindow(key: "error.couldntOpenFile", informationArguments: ["\(Playlist.tracks[index].url.path)"], error: error)
            return
        }
        Playlist.tracks[index] = refreshedFileInfo
        tableView.reloadData(forRowIndexes: IndexSet(integer: index), columnIndexes: IndexSet(0..<tableView.tableColumns.count))

        let soundFont: SoundFont
        do {
            soundFont = try SoundFonts.refresh(index: soundFontsPopUp.indexOfSelectedItem)
        } catch {
            showErrorInCurrentWindow(key: "error.couldntLoadSoundFont", informationArguments: [SoundFonts[soundFontsPopUp.indexOfSelectedItem].name], error: error)
            return
        }

        let savePanel = NSSavePanel()
        savePanel.nameFieldStringValue = refreshedFileInfo.url.deletingPathExtension().lastPathComponent + ".wav"
        savePanel.isExtensionHidden = false
        savePanel.allowedFileTypes = ["com.microsoft.waveform-audio"]
        savePanel.begin { result in
            if result != .OK {
                return
            }
            if let url = savePanel.url {
                self.export(file: refreshedFileInfo, to: url, using: soundFont)
            }
        }
    }

    func export(file: MIDIFileInfo, to url: URL, using soundFont: SoundFont) {
        let exporter: MIDIExporter
        do {
            exporter = try MIDIExporter(file: file, soundFont: soundFont)
        } catch {
            showErrorInCurrentWindow(key: "error.couldntCreateExporter", informationArguments: ["\(file.url.path)"], error: error)
            return
        }

        do {
            try exporter.export(to: url, duration: file.duration + Double(Preferences.standard.songPadding))
        } catch {
            showErrorInCurrentWindow(key: "error.couldntExportMIDI", informationArguments: ["\(file.url.path)"], error: error)
        }
    }

}


// MARK: keyboard/mouse input

extension PlayerViewController {

    @objc func handleDoubleClick(_ sender: Any) {
        if tableView.clickedRow != -1 {
            // Double click always starts the clicked song
            loadTrack(tableView.clickedRow)
        }
    }

    override func keyDown(with event: NSEvent) {
        interpretKeyEvents([event])
    }

    @IBAction override func deleteBackward(_ sender: Any?) {
        // Check if a MIDI player exists for one of the songs we're deleting, and kill it if so
        if midiPlayer != nil {
            let track = indexOfActiveSong()!
            if tableView.selectedRowIndexes.contains(track) {
                stopTrack()
            }
        }

        // Delete key removes any selected tracks
        Playlist.tracks.remove(at: tableView.selectedRowIndexes)
        tableView.removeRows(at: tableView.selectedRowIndexes, withAnimation: [.slideUp])
        lastSelectedRowIndexes = IndexSet() // Clear the saved selection used in tableView(_:sortDescriptorsDidChange:oldDescriptors:)
        updateControls()
    }

    override func insertNewline(_ sender: Any?) {
        // Return always starts a song
        playNewSong()
    }

    override func insertText(_ insertString: Any) {
        guard let text = insertString as? String else {
            return
        }
        if text == " " {
            // Space bar behaves like the play button
            playButtonClicked(self)
        }
    }

}
