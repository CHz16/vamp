//
//  TrackInfoViewController.swift
//  VAMP
//
//  Created by 'Ili Butterfield on 12/13/22.
//  Copyright © 2022 'Ili Butterfield. All rights reserved.
//

import AppKit
import Foundation


fileprivate enum TrackInfosCellIdentifier {
    static let trackNumberCell = NSUserInterfaceItemIdentifier(rawValue: "trackNumber")
    static let textCell = NSUserInterfaceItemIdentifier(rawValue: "text")
    static let copyrightCell = NSUserInterfaceItemIdentifier(rawValue: "copyright")
    static let trackNameCell = NSUserInterfaceItemIdentifier(rawValue: "trackName")
    static let instrumentNameCell = NSUserInterfaceItemIdentifier(rawValue: "instrumentName")
    static let lyricsCell = NSUserInterfaceItemIdentifier(rawValue: "lyrics")
    static let markersCell = NSUserInterfaceItemIdentifier(rawValue: "markers")
    static let cuePointsCell = NSUserInterfaceItemIdentifier(rawValue: "cuePoints")
    static let programNameCell = NSUserInterfaceItemIdentifier(rawValue: "programName")
    static let deviceNameCell = NSUserInterfaceItemIdentifier(rawValue: "deviceName")
}

fileprivate let columnMapping = [
    TrackInfosCellIdentifier.textCell: \TrackInfo.text,
    TrackInfosCellIdentifier.copyrightCell: \TrackInfo.copyright,
    TrackInfosCellIdentifier.trackNameCell: \TrackInfo.trackName,
    TrackInfosCellIdentifier.instrumentNameCell: \TrackInfo.instrumentName,
    TrackInfosCellIdentifier.lyricsCell: \TrackInfo.lyrics,
    TrackInfosCellIdentifier.markersCell: \TrackInfo.markers,
    TrackInfosCellIdentifier.cuePointsCell: \TrackInfo.cuePoints,
    TrackInfosCellIdentifier.programNameCell: \TrackInfo.programName,
    TrackInfosCellIdentifier.deviceNameCell: \TrackInfo.deviceName
]


class TrackInfoViewController: NSViewController {

    @IBOutlet weak var trackInfosTableView: NSTableView!
    var measurementTextCell = NSTextFieldCell.init()

    override func viewDidLoad() {
        super.viewDidLoad()

        trackInfosTableView.dataSource = self
        trackInfosTableView.delegate = self
        NotificationCenter.default.addObserver(self, selector: #selector(reloadData), name: .currentTrackInfosUpdated, object: nil)

        // Force a refresh of visible columns when the window opens
        reloadData()
    }

    override var representedObject: Any? {
        didSet {
        // Update the view, if already loaded.
        }
    }

    deinit {
        NotificationCenter.default.removeObserver(self)
    }


    @objc func reloadData() {
        // Go through the track infos and figure out which columns actually have data set, and then only show those
        var seen: [NSUserInterfaceItemIdentifier: Bool] = [:]
        for trackInfo in CurrentTrackInfos.infos {
            for (identifier, keyPath) in columnMapping {
                if trackInfo[keyPath: keyPath] != nil {
                    seen[identifier] = true
                }
            }
        }
        for identifier in columnMapping.keys {
            trackInfosTableView.tableColumns[trackInfosTableView.column(withIdentifier: identifier)].isHidden = (seen[identifier] == nil)
        }

        // Then actually reload the data into the table
        trackInfosTableView.reloadData()
    }

}


// MARK: NSTableViewDataSource

extension TrackInfoViewController: NSTableViewDataSource {

    func numberOfRows(in tableView: NSTableView) -> Int {
        return CurrentTrackInfos.infos.count
    }

}


// MARK: NSTableViewDelegate

extension TrackInfoViewController: NSTableViewDelegate {

    func tableView(_ tableView: NSTableView, viewFor tableColumn: NSTableColumn?, row: Int) -> NSView? {
        guard let identifier = tableColumn?.identifier else {
            return nil
        }
        guard let cell = tableView.makeView(withIdentifier: identifier, owner: self) as? NSTableCellView else {
            return nil
        }

        let trackInfo = CurrentTrackInfos.infos[row]
        if identifier == TrackInfosCellIdentifier.trackNumberCell {
            cell.textField?.stringValue = String(format: "%02d", row)
        } else if let keyPath = columnMapping[identifier] {
            cell.textField?.stringValue = trackInfo[keyPath: keyPath] ?? ""
        }
        return cell
    }

    func tableView(_ tableView: NSTableView, heightOfRow row: Int) -> CGFloat {
        var maxHeight = CGFloat(18.0)
        for (identifier, keyPath) in columnMapping {
            measurementTextCell.stringValue = CurrentTrackInfos.infos[row][keyPath: keyPath] ?? ""
            let width = tableView.tableColumn(withIdentifier: identifier)!.width
            let bounds = NSRect(x: 0, y: 0, width: width, height: CGFloat.greatestFiniteMagnitude)
            maxHeight = max(maxHeight, measurementTextCell.cellSize(forBounds: bounds).height + 5.0)
        }
        return maxHeight
    }

    func tableViewColumnDidResize(_ notification: Notification) {
        trackInfosTableView.noteHeightOfRows(withIndexesChanged: IndexSet(integersIn: 0..<CurrentTrackInfos.infos.count))
    }

}
