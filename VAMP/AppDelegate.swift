//
//  AppDelegate.swift
//  VAMP
//
//  Created by 'Ili Butterfield on 1/14/20.
//  Copyright © 2020 'Ili Butterfield. All rights reserved.
//

import AppKit
import Foundation


@NSApplicationMain
class AppDelegate: NSObject, NSApplicationDelegate {

    weak var playerViewController: PlayerViewController!
    @IBOutlet weak var playPauseMenuItem: NSMenuItem!

    func applicationDidFinishLaunching(_ aNotification: Notification) {
        playerViewController = NSApp.windows[0].contentViewController! as? PlayerViewController
    }

    func applicationWillTerminate(_ aNotification: Notification) {
        // Insert code here to tear down your application
    }


    func application(_ application: NSApplication, open urls: [URL]) {
        // This gets called before applicationDidFinishLaunching, so if the user is launching the program by opening a file then we need to make sure playerViewController is set here before we try to use it
        // There probably isn't a reason to add a nil check around this or the aDFL setting of this, at worst it'll just be set twice and this isn't expensive
        playerViewController = NSApp.windows[0].contentViewController! as? PlayerViewController
        playerViewController.addURLsToPlaylist(urls, row: Playlist.tracks.count)
    }


    // We'll bind these actions at the application delegate level so we can send them to the PlayerViewController no matter which window is in front
    @IBAction func exportMenuItemSelected(_ sender: Any) {
        playerViewController.exportMIDI()
    }
    
    @IBAction func playMenuItemSelected(_ sender: Any) {
        playerViewController.playButtonClicked(self)
    }

    @IBAction func stopMenuItemSelected(_ sender: Any) {
        playerViewController.stopButtonClicked(self)
    }

    @IBAction func previousMenuItemSelected(_ sender: Any) {
        playerViewController.goToPrevious()
    }

    @IBAction func nextMenuItemSelected(_ sender: Any) {
        playerViewController.goToNext()
    }

    @IBAction func increaseVolumeMenuItemSelected(_ sender: Any) {
        playerViewController.increaseVolume()
    }

    @IBAction func decreaseVolumeMenuItemSelected(_ sender: Any) {
        playerViewController.decreaseVolume()
    }

    func setPlayPauseMenuItemTitle(_ title: String) {
        playPauseMenuItem.title = title
    }


}

extension AppDelegate: NSUserInterfaceValidations {

    func validateUserInterfaceItem(_ item: NSValidatedUserInterfaceItem) -> Bool {
        return playerViewController.validateUserInterfaceItem(item)
    }

}
