//
//  SoundFontNameCellTextField.swift
//  VAMP
//
//  Created by 'Ili Butterfield on 1/29/20.
//  Copyright © 2020 'Ili Butterfield. All rights reserved.
//

import AppKit


class SoundFontNameCellTextField: NSTextField {

    var overrideSoundFontName: String?

    override func becomeFirstResponder() -> Bool {
        if let overrideSoundFontName = overrideSoundFontName {
            self.stringValue = overrideSoundFontName
        }
        return super.becomeFirstResponder()
    }

}
