//
//  PreferencesViewController.swift
//  VAMP
//
//  Created by 'Ili Butterfield on 1/23/20.
//  Copyright © 2020 'Ili Butterfield. All rights reserved.
//

import AppKit
import Foundation


fileprivate enum SoundFontsCellIdentifier {
    static let nameCell = NSUserInterfaceItemIdentifier(rawValue: "name")
    static let filenameCell = NSUserInterfaceItemIdentifier(rawValue: "filename")
    static let showInFinderCell = NSUserInterfaceItemIdentifier(rawValue: "showInFinder")
}

fileprivate extension NSPasteboard.PasteboardType {
    static let soundFontPasteboardType = NSPasteboard.PasteboardType(rawValue: "com.quiteajolt.VAMP.soundfont-row")
}


class PreferencesViewController: NSViewController {

    @IBOutlet weak var songPaddingSlider: NSSlider!
    @IBOutlet weak var soundFontsTableView: NSTableView!
    @IBOutlet weak var deleteSoundFontButton: NSButton!

    var lastSelectedRowIndexes = IndexSet()

    override func viewDidLoad() {
        super.viewDidLoad()

        NotificationCenter.default.addObserver(self, selector: #selector(refreshSoundFont(_:)), name: .soundFontRefreshed, object: nil)

        songPaddingSlider.intValue = Int32(Preferences.standard.songPadding)

        soundFontsTableView.dataSource = self
        soundFontsTableView.delegate = self

        soundFontsTableView.registerForDraggedTypes([.URL, .soundFontPasteboardType])

        soundFontsTableView.tableColumn(withIdentifier: SoundFontsCellIdentifier.nameCell)?.sortDescriptorPrototype = NSSortDescriptor(key: SoundFontsCellIdentifier.nameCell.rawValue, ascending: true)
        soundFontsTableView.tableColumn(withIdentifier: SoundFontsCellIdentifier.filenameCell)?.sortDescriptorPrototype = NSSortDescriptor(key: SoundFontsCellIdentifier.filenameCell.rawValue, ascending: true)

        for i in 0..<SoundFonts.count {
            _ = try? SoundFonts.refresh(index: i)
        }
    }

    override var representedObject: Any? {
        didSet {
        // Update the view, if already loaded.
        }
    }

    deinit {
        NotificationCenter.default.removeObserver(self)
    }


}


// MARK: UI - controls

extension PreferencesViewController {

    @IBAction func songPaddingSliderChanged(_ sender: Any) {
        Preferences.standard.songPadding = Int(songPaddingSlider.intValue)
    }

    @IBAction func nameFieldEdited(_ sender: Any) {
        let label = sender as! NSTextField
        let row = soundFontsTableView.row(for: label)
        if label.stringValue != "" {
            SoundFonts.changeName(of: row, to: label.stringValue)
        }
        soundFontsTableView.reloadData(forRowIndexes: IndexSet(integer: row), columnIndexes: IndexSet(0..<soundFontsTableView.tableColumns.count)) // reload the name if we undid the rename, and the filename in case the user changed the name of the system soundfont
    }

    @IBAction func revealInFinderButtonClicked(_ sender: Any) {
        let row = soundFontsTableView.row(for: sender as! NSButton)
        showSoundFontsInFinder(indexes: IndexSet(integer: row))
    }

    @objc func refreshSoundFont(_ notification: Notification) {
        let index = notification.userInfo!["index"]! as! Int
        soundFontsTableView.reloadData(forRowIndexes: IndexSet(integer: index), columnIndexes: IndexSet(0..<soundFontsTableView.tableColumns.count))
    }

}


// MARK: UI - menus

extension PreferencesViewController {

    @IBAction func showInFinderMenuItemSelected(_ sender: Any) {
        showSoundFontsInFinder(indexes: soundFontsTableView.selectedRowIndexes)
    }

    @IBAction func copy(_ sender: Any) {
        let nameFirst = soundFontsTableView.tableColumns[1].identifier == SoundFontsCellIdentifier.nameCell

        var soundFontRows: [String] = []
        for i in soundFontsTableView.selectedRowIndexes {
            let _ = try? SoundFonts[i].refresh()
            let soundFont = SoundFonts[i]
            if nameFirst {
                soundFontRows.append("\(soundFont.name)\t\(soundFont.filename)")
            } else {
                soundFontRows.append("\(soundFont.filename)\t\(soundFont.name)")
            }
        }

        let pasteboard = NSPasteboard.general
        pasteboard.clearContents()
        pasteboard.setString(soundFontRows.joined(separator: "\n"), forType: .string)
    }

    @IBAction func delete(_ sender: Any) {
        deleteBackward(self)
    }

}


// MARK: NSUserInterfaceValidations

extension PreferencesViewController: NSUserInterfaceValidations {

    func validateUserInterfaceItem(_ item: NSValidatedUserInterfaceItem) -> Bool {
        guard let action = item.action else {
            return false
        }

        // When the user right clicks in the table view, we want to select the row they clicked on as the target for the actions. This function... seems like the best place to do it?
        // We'll also deselect any rows if the user right clicks in a row in the table that doesn't exist. However, since this function is also called to check items in the main menu, tableView.clickedRow will be -1 during those validations too. So we'll check if we're showing the table contextual menu by looking at the tag of the item: all items there are tagged with 1
        if soundFontsTableView.clickedRow != -1 {
            soundFontsTableView.selectRowIndexes(IndexSet(integer: soundFontsTableView.clickedRow), byExtendingSelection: false)
        } else if item.tag == 1 {
            soundFontsTableView.selectRowIndexes(IndexSet(), byExtendingSelection: false)
        }

        switch action {
        case #selector(showInFinderMenuItemSelected(_:)), #selector(delete), #selector(copy(_:)):
            return !soundFontsTableView.selectedRowIndexes.isEmpty
        default:
            return true
        }
    }

}


// MARK: file handling

extension PreferencesViewController {

    @IBAction func openDocument(_ sender: Any) {
        let openPanel = NSOpenPanel()
        openPanel.allowsMultipleSelection = true
        openPanel.allowedFileTypes = ["com.soundblaster.soundfont", "public.downloadable-sound"]
        openPanel.begin { result in
            if result != .OK {
                return
            }
            self.addURLsToSoundFonts(openPanel.urls, row: SoundFonts.count)
        }
    }

    func addURLsToSoundFonts(_ urls: [URL], row: Int) {
        var newSoundFonts: [SoundFont] = []
        for url in urls {
            do {
                newSoundFonts.append(try SoundFont(url: url))
            } catch {
                continue
            }
        }

        if !newSoundFonts.isEmpty {
            SoundFonts.insert(contentsOf: newSoundFonts, at: row)
            soundFontsTableView.insertRows(at: IndexSet(row..<(row + newSoundFonts.count)), withAnimation: [.slideDown])
        }
    }

    func showSoundFontsInFinder(indexes: IndexSet) {
        var urls: [URL] = []
        for index in indexes {
            do {
                let refreshedSoundFont = try SoundFonts.refresh(index: index)
                urls.append(refreshedSoundFont.url)
            } catch {
                showErrorInCurrentWindow(key: "error.couldntLoadSoundFont", informationArguments: [SoundFonts[index].name], error: error)
            }
        }
        if !urls.isEmpty {
            NSWorkspace.shared.activateFileViewerSelecting(urls)
        }
    }

}


// MARK: keyboard/mouse input

extension PreferencesViewController {

    override func keyDown(with event: NSEvent) {
        interpretKeyEvents([event])
    }

    @IBAction override func deleteBackward(_ sender: Any?) {
        // Delete key removes any selected soundfonts
        let deletedIndexes = SoundFonts.remove(at: soundFontsTableView.selectedRowIndexes)
        let triedToDeleteSystemSoundFont = (soundFontsTableView.selectedRowIndexes.count != deletedIndexes.count)
        soundFontsTableView.removeRows(at: deletedIndexes, withAnimation: [.slideUp])
        lastSelectedRowIndexes = IndexSet() // Clear the saved selection used in tableView(_:sortDescriptorsDidChange:oldDescriptors:)

        // Show a message if they tried to delete the system soundfont
        if triedToDeleteSystemSoundFont {
            showErrorInCurrentWindow(key: "error.triedToDeleteSystemSoundFont")
        }
    }

    override func insertText(_ insertString: Any) {
        guard let text = insertString as? String else {
            return
        }
        if text == " " {
            // Space doesn't actually seem to trigger the Play/Pause menu option even though it has that as a shortcut, so we'll do this lmao uh oh
            (NSApp.delegate! as! AppDelegate).playMenuItemSelected(self)
        }
    }

}


// MARK: NSTableViewDataSource

extension PreferencesViewController: NSTableViewDataSource {

    func numberOfRows(in tableView: NSTableView) -> Int {
        return SoundFonts.count
    }

    func tableView(_ tableView: NSTableView, sortDescriptorsDidChange oldDescriptors: [NSSortDescriptor]) {
        guard let newSortDescriptor = soundFontsTableView.sortDescriptors.first else {
            return
        }
        guard let newSortDescriptorKey = newSortDescriptor.key else {
            return
        }

        // We want to keep the user's current selection after sorting, but by the time we're in this function, the selection has already been cleared. So, we'll manually save whatever the last selection was so we can use it here.
        // This is a heavy effort because we have to track the changing selection in multiple places lmao. Trapping tableViewSelectionDidChange(_:) isn't an option because it also fires when the sort clears the selection, and it also doesn't fire on all changes we want anyway. Here's the master list of update places:
        // • tableView(_:selectionIndexesForProposedSelection:proposedSelectionIndexes:) - User selecting and deselecting rows
        // • tableView(_:acceptDrop:row:dropOperation:) - User dragging something into the playlist or manually reordering it
        // • deleteBackward(_:) - User deleting row
        // • And of course, this function at the bottom when we update the selection after sorting
        let selectedSoundFontUUIDs = lastSelectedRowIndexes.map { SoundFonts[$0].uuid }

        // Sort the model and then update the table
        if newSortDescriptorKey == SoundFontsCellIdentifier.nameCell.rawValue {
            SoundFonts.sort {
                let ascending = $0.name.localizedStandardCompare($1.name) == .orderedAscending
                return (newSortDescriptor.ascending) ? ascending : !ascending
            }
        } else if newSortDescriptorKey == SoundFontsCellIdentifier.filenameCell.rawValue {
            SoundFonts.sort {
                let ascending = $0.filename.localizedStandardCompare($1.filename) == .orderedAscending
                return (newSortDescriptor.ascending) ? ascending : !ascending
            }
        }
        soundFontsTableView.reloadData()

        // Look up the new indexes of the tracks with the saved UUIDs and make that the new selection
        let newRows = SoundFonts.array.enumerated().filter { selectedSoundFontUUIDs.contains($0.element.uuid) }.map { $0.offset }
        let newSelectionIndexes = IndexSet(newRows)
        lastSelectedRowIndexes = newSelectionIndexes
        soundFontsTableView.selectRowIndexes(newSelectionIndexes, byExtendingSelection: false)
    }

    func tableView(_ tableView: NSTableView, pasteboardWriterForRow row: Int) -> NSPasteboardWriting? {
        let pasteboardItem = NSPasteboardItem()
        pasteboardItem.setPropertyList(row, forType: .soundFontPasteboardType) // lmao using an int as a property list don't worry about it
        return pasteboardItem
    }

}


// MARK: NSTableViewDelegate

extension PreferencesViewController: NSTableViewDelegate {

    func tableView(_ tableView: NSTableView, viewFor tableColumn: NSTableColumn?, row: Int) -> NSView? {
        guard let identifier = tableColumn?.identifier else {
            return nil
        }
        guard let cell = tableView.makeView(withIdentifier: identifier, owner: self) as? NSTableCellView else {
            return nil
        }

        let soundFont = SoundFonts[row]
        if identifier == SoundFontsCellIdentifier.nameCell {
            if !soundFont.isSystemSoundFont {
                cell.textField?.stringValue = soundFont.name
                (cell.textField! as! SoundFontNameCellTextField).overrideSoundFontName = nil
            } else {
                let systemSoundFontDefaultName = NSLocalizedString("systemSoundFontDefaultName", comment: "default system soundfont name")
                cell.textField?.stringValue = soundFont.name + (soundFont.name != systemSoundFontDefaultName ? " [\(systemSoundFontDefaultName)]" : "")
                (cell.textField! as! SoundFontNameCellTextField).overrideSoundFontName = soundFont.name
            }
        } else if identifier == SoundFontsCellIdentifier.filenameCell {
            cell.textField?.stringValue = soundFont.filename
        } else if identifier == SoundFontsCellIdentifier.showInFinderCell {
            // there's no setup for this cell
        }
        return cell
    }

    func tableView(_ tableView: NSTableView, selectionIndexesForProposedSelection proposedSelectionIndexes: IndexSet) -> IndexSet {
        // Save the new selection for use in tableView(_:sortDescriptorsDidChange:oldDescriptors:)
        lastSelectedRowIndexes = proposedSelectionIndexes
        return proposedSelectionIndexes
    }

    func tableViewSelectionDidChange(_ notification: Notification) {
        // Enable/disable the delete song button based on whether anything's selected or not
        // This hook should fire even if we change the selection programmatically (such as when we delete something), which is ideal
        deleteSoundFontButton.isEnabled = (soundFontsTableView.selectedRowIndexes.count > 0)
    }

}


// MARK: NSDraggingDestination

extension PreferencesViewController: NSDraggingDestination {

    func makeURLs(from info: NSDraggingInfo) -> [URL]? {
        return info.draggingPasteboard.readObjects(forClasses: [NSURL.self], options: [.urlReadingContentsConformToTypes: ["com.soundblaster.soundfont", "public.downloadable-sound"]]) as? [URL]
    }

    func tableView(_ tableView: NSTableView, shouldReorderColumn columnIndex: Int, toColumn newColumnIndex: Int) -> Bool {
        // Don't allow the first column (the show in Finder button) to be moved
        return (columnIndex != 0 && newColumnIndex != 0)
    }

    func tableView(_ tableView: NSTableView, draggingSession session: NSDraggingSession, willBeginAt screenPoint: NSPoint, forRowIndexes rowIndexes: IndexSet) {
        // This code assumes that the order the dragging items are processed is the same as the order of the indexes in Array(rowIndexes), which I'm not necessarily sure is a safe assumption, but it seems to work after quick testing
        let nameColumn = tableView.column(withIdentifier: SoundFontsCellIdentifier.nameCell)
        let indexArray = Array(rowIndexes) // convert this to an array so we can index into it in the following closure
        session.enumerateDraggingItems(options: .concurrent, for: tableView, classes: [NSPasteboardItem.self], searchOptions: [:]) { (draggingItem, idx, stop) in
            if let view = tableView.view(atColumn: nameColumn, row: indexArray[idx], makeIfNecessary: false) as? NSTableCellView {
                draggingItem.imageComponentsProvider = { view.draggingImageComponents }
            }
        }
    }

    func tableView(_ tableView: NSTableView, validateDrop info: NSDraggingInfo, proposedRow row: Int, proposedDropOperation dropOperation: NSTableView.DropOperation) -> NSDragOperation {
        guard let firstItem = info.draggingPasteboard.pasteboardItems?[0] else {
            return []
        }

        // Check if the user is dragging a row in the table
        if firstItem.data(forType: .soundFontPasteboardType) != nil {
            if dropOperation != .above {
                return []
            }
            return .move
        }

        // Otherwise, they're dragging files
        guard let urls = makeURLs(from: info) else {
            return []
        }
        if urls.isEmpty || dropOperation != .above {
            return []
        }
        return .move
    }

    func tableView(_ tableView: NSTableView, acceptDrop info: NSDraggingInfo, row: Int, dropOperation: NSTableView.DropOperation) -> Bool {
        guard let firstItem = info.draggingPasteboard.pasteboardItems?[0] else {
            return false
        }

        // Check if the user is dragging a row in the table
        if firstItem.data(forType: .soundFontPasteboardType) != nil {
            guard let draggedRows = info.draggingPasteboard.pasteboardItems?.compactMap({ $0.propertyList(forType: .soundFontPasteboardType) as? Int }) else {
                return false
            }
            if draggedRows.isEmpty {
                return false
            }

            // For reordering multiple selections, what we'll do is put the entire selection, in order, at the spot the user drags it to.
            // So for the model, what we'll do is collect every one of those items into an array, delete each of those items from the original array, and then insert the new array into the insertion spot.
            // The tricky part is that, if we delete items from before the insertion spot, then the insertion spot will change indexes. So to handle that, we'll count how many of those dragged items were before the insertion spot and then subtract that from the insertion spot.
            // We copy the array here and set it later instead of doing SoundFonts.remove and SoundFonts.insert as two separate steps, because the defaults would synchronize between the calls and erase the player's selected soundfont if it was one of the ones being moved
            var soundFontsCopy = SoundFonts.array
            let draggedItems = draggedRows.map { soundFontsCopy[$0] }
            soundFontsCopy.remove(at: IndexSet(draggedRows))
            let insertRow = row - draggedRows.filter { $0 < row }.count
            soundFontsCopy.insert(contentsOf: draggedItems, at: insertRow)
            SoundFonts.array = soundFontsCopy

            tableView.reloadData()

            // Set the selection to the new locations of the moved rows and save the new selection for use in tableView(_:sortDescriptorsDidChange:oldDescriptors:)
            // reloadData() clears the current selection, so we have to do this
            let newSelectionIndexes = IndexSet(insertRow..<(insertRow + draggedItems.count))
            tableView.selectRowIndexes(newSelectionIndexes, byExtendingSelection: false)
            lastSelectedRowIndexes = newSelectionIndexes

            return true
        }

        // Otherwise, they're dragging files
        guard let urls = makeURLs(from: info) else {
            return false
        }
        if urls.isEmpty {
            return false
        }
        addURLsToSoundFonts(urls, row: row)

        // Save the new selection for use in tableView(_:sortDescriptorsDidChange:oldDescriptors:)
        // If the user dragged a file above the selection, then its position shifted.
        lastSelectedRowIndexes = soundFontsTableView.selectedRowIndexes

        return true
    }

}
