//
//  SoundFonts.swift
//  VAMP
//
//  Created by 'Ili Butterfield on 1/23/20.
//  Copyright © 2020 'Ili Butterfield. All rights reserved.
//

import Foundation


extension NSNotification.Name {
    static let soundFontRefreshed = NSNotification.Name(rawValue: "com.quiteajolt.VAMP.soundFontRefreshed")
}


// This is encapsulated like this so that I can make sure it's always synchronized with the defaults. I'm 100% guaranteed to forget to sync it at some point in the code if I don't enforce it like this.

struct SoundFonts {

    private static var soundFonts: [SoundFont] = Preferences.standard.soundFonts

    static var array: [SoundFont] {
        get {
            SoundFonts.soundFonts
        }
        set(newSoundFonts) {
            SoundFonts.soundFonts = newSoundFonts
            Preferences.standard.soundFonts = SoundFonts.soundFonts
        }
    }

    static var count: Int { SoundFonts.soundFonts.count }

    static var indexOfSystemSoundFont: Int {
        for (i, soundFont) in SoundFonts.soundFonts.enumerated() {
            if soundFont.isSystemSoundFont {
                return i
            }
        }
        return -1 // pls ensure this never happens
    }

    static func indexOfSoundFontWithUUID(_ uuid: UUID) -> Int? {
        return SoundFonts.soundFonts.firstIndex { $0.uuid == uuid }
    }

    static subscript(index: Int) -> SoundFont {
        return soundFonts[index]
    }

    static func insert<C>(contentsOf newElements: C, at i: Int) where C: Collection, C.Element == SoundFont {
        SoundFonts.soundFonts.insert(contentsOf: newElements, at: i)
        Preferences.standard.soundFonts = SoundFonts.soundFonts
    }

    // Since the system soundfont can't be removed, this function returns the indexes that were removed
    static func remove(at indexes: IndexSet) -> IndexSet {
        var indexes = indexes
        indexes.remove(SoundFonts.indexOfSystemSoundFont)
        SoundFonts.soundFonts.remove(at: indexes)
        Preferences.standard.soundFonts = SoundFonts.soundFonts
        return indexes
    }

    static func changeName(of index: Int, to newName: String) {
        SoundFonts.soundFonts[index].name = newName
        Preferences.standard.soundFonts = SoundFonts.soundFonts
    }

    static func sort(by areInIncreasingOrder: (SoundFont, SoundFont) throws -> Bool) rethrows {
        try SoundFonts.soundFonts.sort(by: areInIncreasingOrder)
        Preferences.standard.soundFonts = SoundFonts.soundFonts
    }

    static func refresh(index: Int) throws -> SoundFont {
        let refreshedSoundFont = try SoundFonts.soundFonts[index].refresh()
        SoundFonts.soundFonts[index] = refreshedSoundFont
        Preferences.standard.soundFonts = SoundFonts.soundFonts
        NotificationCenter.default.post(name: .soundFontRefreshed, object: self, userInfo: ["index": index])
        return refreshedSoundFont
    }

}
