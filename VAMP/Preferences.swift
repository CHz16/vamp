//
//  Preferences.swift
//  VAMP
//
//  Created by 'Ili Butterfield on 1/22/20.
//  Copyright © 2020 'Ili Butterfield. All rights reserved.
//

import AppKit
import Foundation


enum PreferenceKey {
    static let volume = "volume"
    static let songPadding = "songPadding"
    static let soundFonts = "soundFonts"
    static let selectedSoundFontUUID = "selectedSoundFontUUID"
}

extension NSNotification.Name {
    static let soundFontsModified = NSNotification.Name(rawValue: "com.quiteajolt.VAMP.soundFontsModifiedNotification")
}


class Preferences: NSObject {

    static var standard = Preferences()

    override init() {
        super.init()

        UserDefaults.standard.register(defaults: [
            PreferenceKey.volume: 100.0,
            PreferenceKey.songPadding: 2
        ])

        // We always want to make sure a system soundfont is available, so we'll just create one and forcibly add it to the defaults instead of creating one in the registration dictionary every launch
        if UserDefaults.standard.object(forKey: PreferenceKey.soundFonts) == nil {
            do {
                let systemSoundFont = try SoundFont()
                soundFonts = [systemSoundFont]
                selectedSoundFontUUID = systemSoundFont.uuid
            } catch {
                let alert = NSAlert()
                alert.icon = NSImage(named: NSImage.cautionName)
                alert.messageText = NSLocalizedString("error.couldntFindSystemSoundFont.message", comment: "")
                alert.informativeText = NSLocalizedString("error.couldntFindSystemSoundFont.information", comment: "")
                alert.runModal()
                NSApp.terminate(self)
            }
        }
    }

    @objc dynamic var volume: Double {
        get {
            UserDefaults.standard.double(forKey: PreferenceKey.volume)
        }
        set(newVolume) {
            UserDefaults.standard.set(newVolume, forKey: PreferenceKey.volume)
        }
    }

    @objc dynamic var songPadding: Int {
        get {
            UserDefaults.standard.integer(forKey: PreferenceKey.songPadding)
        }
        set(newSongPadding) {
            UserDefaults.standard.set(newSongPadding, forKey: PreferenceKey.songPadding)
        }
    }

    // Not KVO-able because SoundFont is a struct
    // We'll just send out a good ol' fashioned notification on modification instead
    var soundFonts: [SoundFont] {
        get {
            let data = UserDefaults.standard.data(forKey: PreferenceKey.soundFonts)!
            return try! PropertyListDecoder().decode(Array<SoundFont>.self, from: data)
        }
        set(newSoundFonts) {
            UserDefaults.standard.set(try! PropertyListEncoder().encode(newSoundFonts), forKey: PreferenceKey.soundFonts)
            NotificationCenter.default.post(name: .soundFontsModified, object: self)
        }
    }

    @objc dynamic var selectedSoundFontUUID: UUID {
        get {
            return UUID(uuidString: UserDefaults.standard.string(forKey: PreferenceKey.selectedSoundFontUUID)!)!
        }
        set(newSelectedSoundFontUUID) {
            UserDefaults.standard.set(newSelectedSoundFontUUID.uuidString, forKey: PreferenceKey.selectedSoundFontUUID)
        }
    }

}
